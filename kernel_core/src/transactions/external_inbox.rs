// SPDX-FileCopyrightText: 2022 TriliTech <contact@trili.tech>
//
// SPDX-License-Identifier: MIT

//! Processing external inbox messages - withdrawals & transactions.

use crate::inbox::v1::verifiable::VerifiedTransaction;
use crate::inbox::ExternalInboxMessage;
use crate::inbox::ParsedExternalInboxMessage;
use crate::memory::Accounts;
use crate::storage::AccountStorage;
use crate::transactions::withdrawal::Withdrawal;
use debug::debug_msg;
use host::rollup_core::RawRollupCore;

/// Possible outcomes when processing messages
pub enum ProcessedOutcome {
    /// A verified transaction
    Transaction(VerifiedTransaction),
    /// Call again for the next transaction
    More,
    /// Finished processing transactions
    Finished,
}

/// Prepare an external message for processing, in a stepwise manner.
///
/// Returns a closure, that may be called to perform the next step of
/// processing an external message.
pub fn prepare_for_processing<'a, Host>(
    message: ExternalInboxMessage<'a>,
    accounts: &mut Accounts,
) -> Option<impl FnMut(&Accounts) -> ProcessedOutcome + 'a>
where
    Host: RawRollupCore,
{
    let external_message = parse_external::<Host>(message)?;
    match external_message {
        ParsedExternalInboxMessage::V1(batch) => {
            if let Err(e) = batch.verify_signature::<Host>(accounts) {
                debug_msg!(Host, "Signature verification of batch failed: {}", e);
                return None;
            }
            let mut transactions = batch.transactions.into_iter();
            let step_fn = move |accounts: &Accounts| {
                let outcome = transactions.next().map(|t| t.verify(accounts));
                match outcome {
                    None => ProcessedOutcome::Finished,
                    Some(Ok(verified_transaction)) => {
                        ProcessedOutcome::Transaction(verified_transaction)
                    }
                    Some(Err(err)) => {
                        debug_msg!(Host, "Could not verify transaction: {}", err);

                        ProcessedOutcome::More
                    }
                }
            };
            Some(step_fn)
        }
        ParsedExternalInboxMessage::DAC(_) =>
        // TODO: mechanism to expose DAC committee pks
        {
            todo!()
        }
    }
}

/// Process external message using durable storage directly
pub fn process_external<'a, Host: RawRollupCore>(
    host: &mut Host,
    account_storage: &mut AccountStorage,
    message: ExternalInboxMessage<'a>,
) -> Vec<Vec<Withdrawal>> {
    let batch = match parse_external::<Host>(message) {
        Some(ParsedExternalInboxMessage::V1(batch)) => batch,
        Some(ParsedExternalInboxMessage::DAC(_)) => todo!(),
        None => return vec![],
    };

    let mut all_withdrawals: Vec<Vec<Withdrawal>> = Vec::new();

    if let Err(e) = batch.verify_signature_durable(host, account_storage) {
        debug_msg!(Host, "Signature verification of batch failed: {}", e);
        all_withdrawals
    } else {
        for transaction in batch.transactions.into_iter() {
            if account_storage.begin_transaction(host).is_err() {
                debug_msg!(Host, "Failed to begin transaction");
                continue;
            }

            match transaction.execute(host, account_storage) {
                Ok(withdrawals) => {
                    if account_storage.commit(host).is_err() {
                        panic!("Failed to commit transaction");
                    }

                    all_withdrawals.push(withdrawals);
                }
                Err(err) => {
                    debug_msg!(Host, "Could not execute transaction: {}", err);
                    if account_storage.rollback(host).is_err() {
                        panic!("Failed to rollback transaction");
                    }
                }
            };
        }

        all_withdrawals
    }
}

/// Parse external message, logging error if it occurs.
fn parse_external<Host: RawRollupCore>(
    message: ExternalInboxMessage,
) -> Option<ParsedExternalInboxMessage> {
    match ParsedExternalInboxMessage::parse(message.0) {
        Ok((remaining, external)) => {
            if !remaining.is_empty() {
                debug_msg!(
                    Host,
                    "External message had unused remaining bytes: {:?}",
                    remaining
                );
            }
            Some(external)
        }
        Err(err) => {
            debug_msg!(Host, "Error parsing external message payload {}", err);
            None
        }
    }
}
