// SPDX-FileCopyrightText: 2022 TriliTech <contact@trili.tech>
// SPDX-FileCopyrightText: 2022 Nomadic Labs <contact@nomadic-labs.com>
//
// SPDX-License-Identifier: MIT

//! Contains core logic of the SCORU wasm kernel.
//!
//! The kernel is an implementation of a **TORU-style** *transactions* kernel.
#![deny(missing_docs)]
#![deny(rustdoc::all)]
#![forbid(unsafe_code)]

extern crate alloc;

pub mod bls;
pub mod deposit;
pub mod encoding;
pub mod inbox;
pub mod memory;
pub mod outbox;
pub mod storage;
pub mod transactions;

#[cfg(feature = "testing")]
pub(crate) mod fake_hash;

use deposit::DepositError;
use inbox::DepositFromInternalPayloadError;
use tezos_encoding::nom::error::DecodeError;
use thiserror::Error;

use debug::debug_msg;
use host::input::Input;
use host::rollup_core::{
    RawRollupCore, MAX_INPUT_MESSAGE_SIZE, MAX_INPUT_SLOT_DATA_CHUNK_SIZE,
};
use host::runtime::Runtime;

use crate::inbox::{InboxDeposit, InboxMessage, InternalInboxMessage, Transfer};
use crate::storage::{
    deposit_ticket_to_storage, init_account_storage, AccountStorage, AccountStorageError,
};
use crate::transactions::external_inbox;
use crate::transactions::withdrawal::process_withdrawals;

const MAX_READ_INPUT_SIZE: usize =
    if MAX_INPUT_MESSAGE_SIZE > MAX_INPUT_SLOT_DATA_CHUNK_SIZE {
        MAX_INPUT_MESSAGE_SIZE
    } else {
        MAX_INPUT_SLOT_DATA_CHUNK_SIZE
    };

/// Entrypoint of the *transactions* kernel.
pub fn transactions_run<Host: RawRollupCore>(host: &mut Host) {
    let mut account_storage = match init_account_storage() {
        Ok(v) => v,
        Err(err) => {
            debug_msg!(Host, "Could not get account storage: {:?}", err);
            return;
        }
    };

    if let Ok(Some(input)) = host.read_input(MAX_READ_INPUT_SIZE) {
        match input {
            Input::Message(message) => {
                debug_msg!(
                    Host,
                    "Processing MessageData {} at level {}",
                    message.id,
                    message.level
                );

                if let Err(err) =
                    process_inbox_message(host, &mut account_storage, message.as_ref())
                {
                    debug_msg!(Host, "Error processing header payload {}", err);
                }
            }
            Input::Slot(_message) => todo!("handle slot message"),
        }

        if host.mark_for_reboot().is_err() {
            debug_msg!(Host, "Could not mark host for reboot");
        }
    }
}

#[derive(Error, Debug)]
enum TransactionError<'a> {
    #[error("unable to parse header inbox message {0}")]
    MalformedInboxMessage(nom::Err<DecodeError<&'a [u8]>>),
    #[error("Failed to deposit ticket: {0}")]
    Deposit(#[from] DepositError),
    #[error("Invalid internal inbox message, expected deposit: {0}")]
    InvalidInternalInbox(#[from] DepositFromInternalPayloadError),
    #[error("Error storing ticket on rollup")]
    StorageError(#[from] AccountStorageError),
}

fn process_inbox_message<'a, Host: RawRollupCore>(
    host: &mut Host,
    account_storage: &mut AccountStorage,
    inbox_message: &'a [u8],
) -> Result<(), TransactionError<'a>> {
    let (remaining, message) = InboxMessage::parse(inbox_message)
        .map_err(TransactionError::MalformedInboxMessage)?;

    match message {
        InboxMessage::Internal(InternalInboxMessage::Transfer(Transfer {
            payload,
            ..
        })) => {
            let InboxDeposit {
                destination,
                ticket,
            } = payload.try_into()?;

            deposit_ticket_to_storage(host, account_storage, &destination, &ticket)?;

            debug_assert!(remaining.is_empty());

            Ok(())
        }

        InboxMessage::Internal(
            InternalInboxMessage::StartOfLevel | InternalInboxMessage::EndOfLevel,
        ) => Ok(()),

        InboxMessage::External(message) => {
            external_inbox::process_external(host, account_storage, message)
                .into_iter()
                .for_each(|withdrawals| process_withdrawals(host, withdrawals));
            Ok(())
        }
    }
}

/// Define the `kernel_run` for the transactions kernel.
#[cfg(feature = "tx-kernel")]
pub mod tx_kernel {
    use crate::transactions_run;
    use kernel::kernel_entry;
    kernel_entry!(transactions_run);
}

#[cfg(test)]
mod test {
    use crate::encoding::{
        contract::Contract,
        micheline::MichelineString,
        michelson::{MichelsonContract, MichelsonPair},
        public_key_hash::PublicKeyHash,
        smart_rollup::SmartRollupAddress,
    };

    use super::*;

    use crypto::hash::{ContractKt1Hash, HashTrait, Layer2Tz4Hash};
    use host::rollup_core::Input;
    use mock_runtime::host::MockHost;
    use tezos_encoding::enc::BinWriter;

    #[ignore]
    #[test]
    fn deposit_ticket() {
        // Arrange
        let mut mock_runtime = MockHost::default();

        let destination =
            Layer2Tz4Hash::from_b58check("tz4MSfZsn6kMDczShy8PMeB628TNukn9hi2K").unwrap();

        let ticket_creator =
            Contract::from_b58check("KT1JW6PwhfaEJu6U3ENsxUeja48AdtqSoekd").unwrap();
        let ticket_content = "hello, ticket!";
        let ticket_quantity = 5;

        let message = InboxMessage::Internal(InternalInboxMessage::Transfer(Transfer {
            payload: MichelsonPair(
                MichelineString(destination.to_base58_check()),
                MichelsonPair(
                    MichelsonContract(ticket_creator.clone()),
                    MichelsonPair(
                        MichelineString(ticket_content.to_string()),
                        ticket_quantity.into(),
                    ),
                ),
            ),
            sender: ContractKt1Hash::from_b58check(
                "KT1VsSxSXUkgw6zkBGgUuDXXuJs9ToPqkrCg",
            )
            .unwrap(),
            source: PublicKeyHash::from_b58check("tz3SvEa4tSowHC5iQ8Aw6DVKAAGqBPdyK1MH")
                .unwrap(),
            destination: SmartRollupAddress::from_b58check(
                "scr1HLXM32GacPNDrhHDLAssZG88eWqCUbyLF",
            )
            .unwrap(),
        }));

        let mut input = Vec::new();
        message.bin_write(&mut input).unwrap();

        let level = 10;
        mock_runtime.as_mut().set_ready_for_input(level);
        mock_runtime
            .as_mut()
            .add_next_inputs(10, vec![(Input::MessageData, input)].iter());

        // Act
        transactions_run(&mut mock_runtime);

        // Assert
        // TODO use the accounts in durable storage instead
        /*
        let memory = Memory::load_memory(&mock_runtime);
        let account = memory
            .accounts()
            .account_of(&destination)
            .cloned()
            .expect("Account should be created");

        let mut expected_account = Account::default();
        let expected_ticket = StringTicket::new(
            ticket_creator,
            ticket_content.to_string(),
            ticket_quantity as u64,
        );
        expected_account
            .add_ticket(expected_ticket.identify().unwrap(), ticket_quantity as u64)
            .unwrap();

        assert_eq!(expected_account, account);
        */
    }

    #[test]
    fn deposit_fails_on_invalid_destination() {
        // Arrange
        let mut mock_runtime = MockHost::default();

        let destination = "tz4BAD";

        let ticket_creator =
            Contract::from_b58check("KT1JW6PwhfaEJu6U3ENsxUeja48AdtqSoekd").unwrap();
        let ticket_content = "hello, ticket!";
        let ticket_quantity = 5;

        let message = InboxMessage::Internal(InternalInboxMessage::Transfer(Transfer {
            payload: MichelsonPair(
                MichelineString(destination.to_string()),
                MichelsonPair(
                    MichelsonContract(ticket_creator.clone()),
                    MichelsonPair(
                        MichelineString(ticket_content.to_string()),
                        ticket_quantity.into(),
                    ),
                ),
            ),
            sender: ContractKt1Hash::from_b58check(
                "KT1VsSxSXUkgw6zkBGgUuDXXuJs9ToPqkrCg",
            )
            .unwrap(),
            source: PublicKeyHash::from_b58check("tz3SvEa4tSowHC5iQ8Aw6DVKAAGqBPdyK1MH")
                .unwrap(),
            destination: SmartRollupAddress::from_b58check(
                "scr1HLXM32GacPNDrhHDLAssZG88eWqCUbyLF",
            )
            .unwrap(),
        }));

        let mut input = Vec::new();
        message.bin_write(&mut input).unwrap();

        let level = 10;
        mock_runtime.as_mut().set_ready_for_input(level);
        mock_runtime
            .as_mut()
            .add_next_inputs(10, vec![(Input::MessageData, input)].iter());

        // Act
        transactions_run(&mut mock_runtime);

        // TODO use durable storage instead
        // Assert
        /*
        let ticket_identity = StringTicket::new(
            ticket_creator,
            ticket_content.to_string(),
            ticket_quantity as u64,
        )
        .identify()
        .unwrap();

        let memory = Memory::load_memory(&mock_runtime);
        assert!(!memory.ticket_recognized(&ticket_identity));
        */
    }
}
