// SPDX-FileCopyrightText: 2022 TriliTech <contact@trili.tech>
//
// SPDX-License-Identifier: MIT

//! Michelson-ticket encoding.

use std::fmt::Debug;

use core::fmt::{Display, Formatter, Result as FmtResult};
use crypto::blake2b::{digest_256, Blake2bError};
use hex::FromHexError;
use num_bigint::{BigInt, TryFromBigIntError};
use serde::{Deserialize, Serialize};
use tezos_encoding::enc::{self, BinError, BinWriter};
use tezos_encoding::types::Zarith;
use thiserror::Error;

use super::contract::Contract;
use super::micheline::{MichelineInt, MichelineString};
use super::michelson::{MichelsonContract, MichelsonPair};

#[cfg(feature = "testing")]
pub mod testing;

pub(crate) type StringTicketRepr =
    MichelsonPair<MichelsonContract, MichelsonPair<MichelineString, MichelineInt>>;

/// The hash of a string ticket - identifying a ticket by creator and contents.
#[derive(Clone, PartialEq, Eq, PartialOrd, Ord, Serialize, Deserialize)]
#[serde(try_from = "String", into = "String")]
pub struct StringTicketHash(Vec<u8>);

impl Debug for StringTicketHash {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "TicketId(")?;
        for &byte in self.0.iter() {
            write!(f, "{:02x?}", byte)?;
        }
        write!(f, ")")
    }
}

impl Display for StringTicketHash {
    fn fmt(&self, f: &mut Formatter<'_>) -> FmtResult {
        write!(f, "{}", hex::encode(&self.0))
    }
}

#[allow(clippy::from_over_into)]
impl Into<String> for StringTicketHash {
    fn into(self) -> String {
        hex::encode(&self.0)
    }
}

impl TryFrom<String> for StringTicketHash {
    type Error = FromHexError;

    fn try_from(value: String) -> Result<Self, Self::Error> {
        hex::decode(value).map(Self)
    }
}

/// Errors occurring when identifying tickets.
#[derive(Error, Debug)]
pub enum TicketHashError {
    /// Unable to serialize ticket creator and contents.
    #[error("Unable to serialize ticket for hashing: {0}")]
    Serialization(#[from] BinError),
    /// Unable to hash serialized ticket.
    #[error("Unable to hash ticket bytes: {0}")]
    Hashing(#[from] Blake2bError),
}

/// Errors occurring when identifying tickets.
#[derive(Error, Debug, Clone)]
pub enum StringTicketError {
    /// Invalid amount in ticket repr.
    #[error("ticket amount out of range of u64 {0}")]
    InvalidAmount(TryFromBigIntError<BigInt>),
}

/// Proof that a ticket-identity matches a ticket.
pub struct TrustlessTicketIdentity(StringTicketHash, StringTicket);

impl TrustlessTicketIdentity {
    /// Break the link between the identity & the ticket.
    pub fn consume(self) -> (StringTicketHash, StringTicket) {
        (self.0, self.1)
    }

    /// Access the identity without breaking the trustless-link.
    pub fn identity(&self) -> &StringTicketHash {
        &self.0
    }

    /// Access the ticket without breaking the trustless-link.
    pub fn ticket(&self) -> &StringTicket {
        &self.1
    }
}

/// Michelson ticket, specialised to string-contents.
///
/// `amount` is guaranteed to always be positive.
#[derive(Debug, PartialEq, Eq)]
pub struct StringTicket {
    pub(crate) creator: Contract,
    pub(crate) contents: String,
    pub(crate) amount: u64,
}

impl StringTicket {
    /// Create a new string ticket
    pub fn new(creator: Contract, contents: String, amount: u64) -> Self {
        Self {
            creator,
            contents,
            amount,
        }
    }

    /// Return an identifying hash of the ticket creator and contents.
    ///
    /// Calculated as the `blake2b` hash of a tezos-encoded `obj2`:
    /// - creator contract
    /// - string contents
    pub fn identify(&self) -> Result<StringTicketHash, TicketHashError> {
        let mut bytes = Vec::new();
        self.creator.bin_write(&mut bytes)?;
        enc::string(&self.contents, &mut bytes)?;

        let digest = digest_256(bytes.as_slice())?;

        Ok(StringTicketHash(digest))
    }

    /// Guarantee that an identity is bound to a ticket.
    pub fn identify_trustless(self) -> Result<TrustlessTicketIdentity, TicketHashError> {
        Ok(TrustlessTicketIdentity(self.identify()?, self))
    }

    /// Return the quantity of this ticket.
    pub fn amount(&self) -> u64 {
        self.amount
    }
}

impl TryFrom<StringTicketRepr> for StringTicket {
    type Error = StringTicketError;

    fn try_from(repr: StringTicketRepr) -> Result<Self, Self::Error> {
        let MichelsonPair(
            MichelsonContract(creator),
            MichelsonPair(MichelineString(contents), MichelineInt(Zarith(amount))),
        ) = repr;

        let amount: u64 = amount
            .try_into()
            .map_err(StringTicketError::InvalidAmount)?;

        Ok(Self::new(creator, contents, amount))
    }
}

impl From<StringTicket> for StringTicketRepr {
    fn from(ticket: StringTicket) -> StringTicketRepr {
        MichelsonPair(
            MichelsonContract(ticket.creator),
            MichelsonPair(
                MichelineString(ticket.contents),
                MichelineInt(Zarith(ticket.amount.into())),
            ),
        )
    }
}
