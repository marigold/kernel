// SPDX-FileCopyrightText: 2022 TriliTech <contact@trili.tech>
//
// SPDX-License-Identifier: MIT

//! External inbox messages - transactions & withdrawals.
//!
//! External inbox messages are the mechanism by which user's interact with the kernel,
//! when either transacting between accounts on the rollup, or withdrawing from the
//! rollup.
//!
//! Mostly equivalent to TORU's [l2_batch](https://gitlab.com/tezos/tezos/-/blob/bbcb6382f1b418ff03283f7f977b32fd681f9099/src/proto_alpha/lib_protocol/tx_rollup_l2_batch.mli), the main differences being:
//! - lack of compact encoding (TODO: [tezos/tezos#3040](https://gitlab.com/tezos/tezos/-/issues/3040))
//! - replaces uses of `Ticket_hash` + `quantity` with `StringTicket`.  In future, either
//!   ticket_hash will be reintroduced, or indexes (with the compact encoding) will be encouraged.

use crypto::hash::Layer2Tz4Hash;
use nom::branch::alt;
use nom::bytes::complete::tag;
use nom::combinator::map;
use nom::sequence::preceded;
use tezos_encoding::enc::BinWriter;
use tezos_encoding::encoding::HasEncoding;
use tezos_encoding::nom::NomReader;

use crate::bls::CompressedPublicKey;

pub mod dac_message;
pub mod sendable;
#[cfg(feature = "testing")]
pub mod testing;
pub mod v1;

/// External inbox message.  Defined to be arbitrary bytes by communication protocol.
///
/// To be parsed & validated into a kernel-specific data structure.
///
/// **NB** this holds a reference to the slice of the inbox message that corresponds to
/// the 'external message' part, rather than copying into a new buffer.
#[derive(Debug, PartialEq, Eq)]
pub struct ExternalInboxMessage<'a>(pub &'a [u8]);

impl BinWriter for ExternalInboxMessage<'_> {
    fn bin_write(&self, output: &mut Vec<u8>) -> tezos_encoding::enc::BinResult {
        output.extend_from_slice(self.0);
        Ok(())
    }
}

/// Upgradeable representation of external inbox messages.
#[derive(Debug, PartialEq, Eq)]
pub enum ParsedExternalInboxMessage<'a> {
    /// Version 1 of operation batching.
    V1(v1::ParsedBatch<'a>),
    /// Dac message.
    DAC(dac_message::ParsedDacMessage),
}

impl<'a> ParsedExternalInboxMessage<'a> {
    const PARSED_BATCH_TAG: u8 = 0;
    const PARSED_DAC_MESSAGE_DAC: u8 = 42;

    /// Parse an external inbox message.
    pub fn parse(input: &'a [u8]) -> tezos_encoding::nom::NomResult<Self> {
        alt((
            map(
                preceded(tag([Self::PARSED_BATCH_TAG]), v1::ParsedBatch::parse),
                ParsedExternalInboxMessage::V1,
            ),
            map(
                preceded(
                    tag([Self::PARSED_DAC_MESSAGE_DAC]),
                    dac_message::ParsedDacMessage::nom_read,
                ),
                ParsedExternalInboxMessage::DAC,
            ),
        ))(input)
    }
}

/// Represents the `signer` of a layer-2 operation.
///
/// This is either a [CompressedPublicKey] or a layer-2 address, whose associated account
/// contains a corresponding BLS public key.
#[derive(Debug, Clone, PartialEq, Eq, HasEncoding, NomReader, BinWriter)]
pub enum Signer {
    /// A signer identified by a BLS public key.
    BlsPublicKey(CompressedPublicKey),
    /// A signer identified by a layer-2 address, in turn associated with a BLS public key.
    Layer2Address(Layer2Tz4Hash),
}

#[cfg(test)]
mod test {
    use tezos_rollup_encoding::testing::{make_preimage_hash, make_witnesses};

    use super::*;
    use proptest::prelude::*;
    use tezos_encoding::enc::BinWriter;
    use tezos_encoding::nom::NomReader;
    use tezos_rollup_encoding::dac::PreimageHash;

    proptest! {
        #[test]
        fn encode_decode_signer(signer in Signer::arb(), remaining in any::<Vec<u8>>()) {
            let mut encoded = Vec::new();
            signer
                .bin_write(&mut encoded)
                .expect("encoding should work");

            encoded.extend_from_slice(remaining.as_slice());

            let (remaining_input, decoded_signer) =
                Signer::nom_read(encoded.as_slice()).expect("decoding should work");

            assert_eq!(remaining.as_slice(), remaining_input);
            assert_eq!(signer, decoded_signer);
        }
    }

    #[test]
    fn parse_dac_message() {
        use crate::bls::CompressedSignature;
        let root_hash = make_preimage_hash("example content".as_bytes());

        // Random aggregated signature generated in tezos.
        let aggregated_signature: [u8; 96] = [
            152, 154, 156, b'V', b'i', b'|', 2, b'\'', b'd', 216, 229, 140, 144, b'8',
            b'x', b'k', b'q', 209, 023, b'6', 217, 021, b';', b' ', b'q', 166, 251, 213,
            b'K', 16, b'.', b'a', 129, b'P', 21, b'x', 162, b'c', b'n', 212, 132, b'\t',
            b'(', b'5', b'H', 253, b'>', 179, 7, 19, 229, 164, b'Z', 158, 197, 220, 200,
            b')', b'\'', b'a', b't', 165, 161, 147, b'8', b'K', 136, b'R', b' ', b']',
            227, b'K', b'G', 227, 225, 169, 244, 019, b'd', 21, 228, 127, 179, 249, 233,
            195, 159, b'[', 163, b'O', b'0', 167, 200, b'#', 213, 243,
        ];
        let witnesses = 1_u8;
        let mut valid_bytes: Vec<u8> = vec![42];
        valid_bytes.extend_from_slice(&root_hash);
        valid_bytes.extend_from_slice(&aggregated_signature);
        valid_bytes.extend_from_slice(&[witnesses]);

        let expected_message =
            ParsedExternalInboxMessage::DAC(dac_message::ParsedDacMessage {
                root_hash: PreimageHash::from(&root_hash),
                aggregated_signature: CompressedSignature::try_from(aggregated_signature)
                    .unwrap(),
                witnesses: make_witnesses(witnesses as usize),
            });
        let (_remaining, actual_message) =
            ParsedExternalInboxMessage::parse(&valid_bytes)
                .expect("The external messsage should be parsable");

        assert_eq!(expected_message, actual_message);
    }
}
