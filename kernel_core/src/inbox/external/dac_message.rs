// SPDX-FileCopyrightText: 2022 Nomadic Labs <contact@nomadic-labs.com>
// SPDX-FileCopyrightText: 2022 TriliTech <contact@trili.tech>

//
// SPDX-License-Identifier: MIT

//! Representation of a DAC message communicating root hashes to the kernel to
//! download pages.

use super::v1::verifiable::VerificationError;
use crate::bls::{CompressedSignature, PublicKey, Signature};
use host::rollup_core::{RawRollupCore, PREIMAGE_HASH_SIZE};
use host::runtime::RuntimeError;
use tezos_encoding::nom::NomReader;
use tezos_encoding::types::Zarith;
use tezos_rollup_encoding::dac::{
    fetch_page_raw, reveal_loop, PreimageHash, V0SliceContentPage, MAX_PAGE_SIZE,
};
use thiserror::Error;

const MAX_DAC_LEVELS: usize = 3;

/// DAC external message is a message sent from DAC to rollups to indicate that there
/// is data ready to be revealed. By the time the kernel receives this message, data
/// should already be available locally and ready to be revealed. The raw structure
/// of the external message is root hash ^ aggregate signature ^ witnesses
#[derive(Debug, PartialEq, Eq, NomReader)]
pub struct ParsedDacMessage {
    /// Root page hash
    pub root_hash: PreimageHash,
    /// Aggregated signature of the DAC committee.
    pub aggregated_signature: CompressedSignature,
    /// Data_encoding.Bit_set.t is actually a Z.t
    pub witnesses: Zarith,
}

impl ParsedDacMessage {
    /// Verifies that parsed_dac_message is valid against the given dac_committee
    pub fn verify_signature(
        &self,
        dac_committee: &[&PublicKey],
    ) -> Result<(), VerificationError> {
        if cfg!(not(feature = "tx-kernel-no-sig-verif")) {
            let sig = Signature::try_from(&self.aggregated_signature)?;
            let root_hash = self.root_hash.as_ref();
            let mut pk_msg_iter =
                dac_committee.iter().enumerate().filter_map(|(i, &member)| {
                    if self.witnesses.0.bit(i as u64) {
                        Some((root_hash.as_slice(), member))
                    } else {
                        None
                    }
                });
            let is_verified = sig.aggregate_verify(&mut pk_msg_iter)?;
            if !is_verified {
                return Err(VerificationError::SignatureVerificationError);
            }
        }
        Ok(())
    }

    /// Reveals all message data referenced by [self.root_hash]. Verifies
    /// that the [self.aggregated_signature] was indeed signed by the DAC committee
    pub fn verify_and_reveal_dac_messages<Host: RawRollupCore>(
        &self,
        host: &mut Host,
        dac_committee: &[&PublicKey],
    ) -> Result<Vec<u8>, RevealDacMessageError> {
        self.verify_signature(dac_committee)
            .map_err(RevealDacMessageError::SignatureVerificationError)
            .and_then(|()| Self::reveal_dac_message(&self.root_hash, host))
    }

    /// Reveals all message data from referenced by [self.root_hash].
    pub fn reveal_dac_message<Host: RawRollupCore>(
        root_hash: &PreimageHash,
        host: &mut Host,
    ) -> Result<Vec<u8>, RevealDacMessageError> {
        let mut result_vec = vec![];
        let mut buffer = [0u8; MAX_PAGE_SIZE * MAX_DAC_LEVELS];
        reveal_loop(
            host,
            0,
            root_hash.as_ref(),
            buffer.as_mut_slice(),
            MAX_DAC_LEVELS,
            &mut Self::write_content(&mut result_vec),
        )
        .map_err(RevealDacMessageError::RevealLoopError)?;
        Ok(result_vec)
    }

    fn write_content<'a, Host: RawRollupCore>(
        buf: &'a mut Vec<u8>,
    ) -> impl FnMut(&mut Host, V0SliceContentPage) -> Result<(), &'static str> + '_ {
        |_, content| {
            buf.extend_from_slice(content.as_ref());
            Ok(())
        }
    }

    /// Reveal dac messages for rollup_id
    pub fn reveal_dac_message_for_rollup<Host: RawRollupCore>(
        &self,
        host: &mut Host,
        rollup_id: u16,
    ) -> Result<Vec<u8>, RevealDacMessageError> {
        let root_hash_for_rollup =
            Self::find_hash_for_rollup(host, self.root_hash.as_ref(), rollup_id)?;
        Self::reveal_dac_message(&root_hash_for_rollup, host)
    }

    fn find_hash_for_rollup<Host: RawRollupCore>(
        host: &Host,
        root_hash: &[u8; PREIMAGE_HASH_SIZE],
        rollup_id: u16,
    ) -> Result<PreimageHash, RevealDacMessageError> {
        let mut buffer = [0u8; MAX_PAGE_SIZE];
        let (page, _) = fetch_page_raw(host, root_hash, &mut buffer)
            .map_err(RevealDacMessageError::RevealPreImageError)?;
        let rollup_hash_offset = rollup_id as usize * PREIMAGE_HASH_SIZE;
        if page.len() < rollup_hash_offset + PREIMAGE_HASH_SIZE {
            return Err(RevealDacMessageError::DecodeError(format!("EOL reached while retrieving hash for rollup {}. Hash page has too few hashes", rollup_id)));
        }
        let root_hash_raw =
            &page[rollup_hash_offset..rollup_hash_offset + PREIMAGE_HASH_SIZE];
        let (_other_hashes, root_hash) = PreimageHash::nom_read(root_hash_raw)
            .map_err(|e| RevealDacMessageError::DecodeError(e.to_string()))?;
        Ok(root_hash)
    }
}
/// parse_dac_message errors
#[derive(Error, Debug)]
pub enum RevealDacMessageError {
    /// Error from the host's low-level reveal_preimage func
    #[error("Reveal pre-image failed: {0:?}")]
    RevealPreImageError(RuntimeError),

    /// Propagate error from SlicePageError
    #[error("{0:?}")]
    RevealLoopError(&'static str),

    /// Propagate error from VerificationError
    #[error(transparent)]
    SignatureVerificationError(VerificationError),

    /// Decode errors from fetching page
    #[error("{0}")]
    DecodeError(String),
}

#[cfg(test)]
mod tests {
    use std::{fs, path::Path};

    use super::*;
    use crate::bls::PublicKey;
    use crate::{
        bls::{BlsKey, CompressedPublicKey, CompressedSignature, Signature},
        inbox::ParsedExternalInboxMessage,
    };
    use crypto::{base58::FromBase58Check, blake2b::digest_256};
    use fake::{Fake, Faker};
    use mock_runtime::host::MockHost;
    use mock_runtime::state::HostState;
    use tezos_rollup_encoding::testing::{make_preimage_hash, make_witnesses};

    #[test]
    fn nom_read_parses_dac_message() {
        let root_hash = make_preimage_hash("example content".as_bytes());
        // Random aggregated signature generated in tezos.
        let aggregated_signature: [u8; 96] = [
            152, 154, 156, b'V', b'i', b'|', 2, b'\'', b'd', 216, 229, 140, 144, b'8',
            b'x', b'k', b'q', 209, 023, b'6', 217, 021, b';', b' ', b'q', 166, 251, 213,
            b'K', 16, b'.', b'a', 129, b'P', 21, b'x', 162, b'c', b'n', 212, 132, b'\t',
            b'(', b'5', b'H', 253, b'>', 179, 7, 19, 229, 164, b'Z', 158, 197, 220, 200,
            b')', b'\'', b'a', b't', 165, 161, 147, b'8', b'K', 136, b'R', b' ', b']',
            227, b'K', b'G', 227, 225, 169, 244, 019, b'd', 21, 228, 127, 179, 249, 233,
            195, 159, b'[', 163, b'O', b'0', 167, 200, b'#', 213, 243,
        ];
        let witnesses = 1;
        let mut valid_bytes = Vec::<u8>::new();
        valid_bytes.extend_from_slice(&root_hash);
        valid_bytes.extend_from_slice(&aggregated_signature);
        valid_bytes.extend_from_slice(&[1]);

        let expected_message = ParsedDacMessage {
            root_hash: PreimageHash::from(&root_hash),
            aggregated_signature: CompressedSignature::try_from(aggregated_signature)
                .unwrap(),
            witnesses: make_witnesses(witnesses),
        };
        let (_remaining, actual_message) =
            ParsedDacMessage::nom_read(&valid_bytes).expect("Parse failure");

        assert_eq!(expected_message, actual_message);
    }
    #[test]
    fn nom_read_fails_invalid_dac_message() {
        let message = b"bound to fail bound to fail bound to fail bound to fail";
        assert!(ParsedDacMessage::nom_read(message).is_err());
    }

    #[test]
    fn verify_root_hash() {
        let root_hash = make_preimage_hash("example content".as_bytes());
        let signatory1: BlsKey = Faker.fake();
        let signatory2: BlsKey = Faker.fake();
        let signatory3: BlsKey = Faker.fake();

        let dac_committee = [
            signatory1.public_key(),
            signatory2.public_key(),
            signatory3.public_key(),
        ];

        let sig2 = signatory2.sign(&root_hash);
        let sig3 = signatory3.sign(&root_hash);
        let witnesses = make_witnesses(2 | 4);

        let aggregated_signature = Signature::aggregate_sigs(&[&sig2, &sig3]).unwrap();
        let aggregated_signature =
            CompressedSignature::try_from(aggregated_signature).unwrap();

        let dac_message = ParsedDacMessage {
            root_hash: PreimageHash::from(&root_hash),
            aggregated_signature,
            witnesses: witnesses.clone(),
        };

        // Verifies that only signatory 2 and 3 signed the message the signature
        // and that aggregated signature is correctly verified.
        let res =
            ParsedDacMessage::verify_signature(&dac_message, dac_committee.as_slice());
        assert!(res.is_ok());

        // Fails when the wrong bitset is provided
        let bad_dac_message = ParsedDacMessage {
            witnesses: make_witnesses(1 | 2 | 4),
            ..dac_message
        };
        let res = ParsedDacMessage::verify_signature(
            &bad_dac_message,
            dac_committee.as_slice(),
        );
        assert!(res.is_err());

        let invalid_hash: [u8; PREIMAGE_HASH_SIZE - 1] =
            digest_256(b"some bad content").unwrap().try_into().unwrap();

        let mut invalid_root_hash = [0_u8; PREIMAGE_HASH_SIZE];
        invalid_root_hash[1..].copy_from_slice(&invalid_hash);

        let bad_dac_message = ParsedDacMessage {
            root_hash: PreimageHash::from(&invalid_root_hash),
            ..dac_message
        };
        // Asert that an aggregated signature of the wrong root hash is rejected
        // by the signature's verification.
        let res = ParsedDacMessage::verify_signature(
            &bad_dac_message,
            dac_committee.as_slice(),
        );
        assert!(res.is_err());
    }

    #[test]
    fn reveal_dac_message() {
        // setup the environment
        let dac_message_dir = Path::new(env!("CARGO_MANIFEST_DIR"));
        let original_content =
            fs::read_to_string(dac_message_dir.join("resources/dac_message/original"))
                .unwrap();
        let external_message = hex::decode(
            fs::read(dac_message_dir.join("resources/dac_message/external_message"))
                .unwrap(),
        )
        .unwrap();

        let signer_pk =
            fs::read_to_string(dac_message_dir.join("resources/dac_message/signer"))
                .unwrap();
        let signer_pk: [u8; 48] = signer_pk.from_base58check().unwrap()[4..]
            .try_into()
            .unwrap();
        let signer_pk =
            PublicKey::try_from(CompressedPublicKey::try_from(signer_pk).unwrap())
                .unwrap();
        let dac_committee = [&signer_pk];

        // prepare host
        let all_pages =
            fs::read_dir(dac_message_dir.join("resources/dac_message/pages")).unwrap();
        let mut host_state = HostState::default();
        for page in all_pages {
            let page = page.unwrap();
            let page = fs::read(page.path()).unwrap();
            let preimage = page.to_vec();
            host_state.set_preimage(preimage);
        }
        let mut host = MockHost::from(host_state);

        // parse message
        if let (_, ParsedExternalInboxMessage::DAC(parsed_dac_message)) =
            ParsedExternalInboxMessage::parse(&external_message).unwrap()
        {
            let retrieve_data = String::from_utf8(
                parsed_dac_message
                    .verify_and_reveal_dac_messages(&mut host, &dac_committee)
                    .unwrap(),
            )
            .unwrap();
            assert_eq!(original_content, retrieve_data);
        } else {
            panic!("External inbox message expected to be a DAC external message");
        }
    }
}
