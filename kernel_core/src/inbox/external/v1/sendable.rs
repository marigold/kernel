// SPDX-FileCopyrightText: 2022 TriliTech <contact@trili.tech>
//
// SPDX-License-Identifier: MIT

//! Sendable/serializable versions of `Parsed*` structs.

use super::{Operation, ToBytesError};
use crate::{
    bls::{BlsKey, CompressedSignature, Signature},
    inbox::Signer,
};
use tezos_encoding::enc::{self, BinError, BinErrorKind, BinWriter};
use tezos_encoding::encoding::{Encoding, HasEncoding};
use tezos_encoding::has_encoding;

/// A list of operations, each of which is attributable to a different signer.
#[derive(Debug, PartialEq, HasEncoding, BinWriter)]
pub struct Transaction {
    #[encoding(dynamic, list)]
    operations: Vec<Operation>,
    #[encoding(skip)]
    keys: Vec<BlsKey>,
}

impl Transaction {
    /// Create a new transaction.
    pub fn new(
        ops_with_keys: impl IntoIterator<Item = (Operation, BlsKey)>,
    ) -> Result<Self, ToBytesError> {
        let (operations, keys) =
            ops_with_keys.into_iter().unzip::<_, _, Vec<_>, Vec<_>>();

        for (op, key) in operations.iter().zip(keys.iter()) {
            let _ok = match &op.signer {
                Signer::BlsPublicKey(pk) if pk != key.compressed_public_key() => {
                    Err(ToBytesError::IncorrectKey)
                }
                Signer::Layer2Address(a) if a != key.public_key_hash() => {
                    Err(ToBytesError::IncorrectKey)
                }
                _ => Ok(()),
            }?;
        }

        Ok(Self { operations, keys })
    }

    /// Get the operations
    pub fn operations(&self) -> &[Operation] {
        self.operations.as_slice()
    }

    /// Serialize a batch of transactions, alongside the aggregate signature.
    ///
    /// Compatible with [VerifiableTransaction::parse].
    ///
    /// [VerifiableTransaction::parse]: super::verifiable::VerifiableTransaction::parse
    pub fn to_bytes_signed(&self) -> Result<(Signature, Vec<u8>), ToBytesError> {
        let mut encoded = Vec::new();
        self.bin_write(&mut encoded)?;

        let signatures = self
            .keys
            .iter()
            .map(|k| k.sign(&encoded))
            .collect::<Vec<_>>();

        let signatures = signatures.iter().collect::<Vec<_>>();

        let sig = Signature::aggregate_sigs(signatures.as_slice())?;

        Ok((sig, encoded))
    }
}

/// A batch of operations, for serialization as an external inbox message.
#[derive(Debug, PartialEq)]
pub struct Batch {
    transactions: Vec<Transaction>,
}

has_encoding!(Batch, SENDABLE_BATCH_ENCODING, { Encoding::Custom });

impl Batch {
    /// Create a new batch from a list of transactions.
    pub fn new(transactions: Vec<Transaction>) -> Self {
        Self { transactions }
    }

    fn to_bytes_signed(
        &self,
        output: &mut Vec<u8>,
    ) -> Result<CompressedSignature, ToBytesError> {
        let signed_bytes = self
            .transactions
            .iter()
            .map(|t| t.to_bytes_signed())
            .collect::<Result<Vec<(_, _)>, _>>()?;

        let signatures = signed_bytes.iter().map(|(sig, _)| sig).collect::<Vec<_>>();
        let sig = Signature::aggregate_sigs(signatures.as_slice())?;
        let compressed_sig = CompressedSignature::from(&sig);

        let output_bytes = signed_bytes
            .into_iter()
            .flat_map(|(_, bytes)| bytes)
            .collect::<Vec<u8>>();

        enc::dynamic(|output_bytes: Vec<u8>, output: &mut Vec<u8>| {
            output.extend(output_bytes);
            Ok(())
        })(output_bytes, output)?;

        Ok(compressed_sig)
    }
}

impl BinWriter for Batch {
    /// Serialize a batch of transactions, including the aggregate signature.
    ///
    /// Compatible with [ParsedBatch::parse].
    ///
    /// [ParsedBatch::parse]: super::ParsedBatch::parse
    fn bin_write(&self, output: &mut Vec<u8>) -> enc::BinResult {
        let sig = self
            .to_bytes_signed(output)
            .map_err(|e| BinError::from(BinErrorKind::CustomError(format!("{}", e))))?;
        sig.bin_write(output)
    }
}

#[cfg(test)]
mod test {

    use proptest::collection;
    use proptest::prelude::*;
    use tezos_encoding::enc::BinWriter;

    use super::Batch;
    use super::Transaction;
    use crate::inbox::v1::Operation;
    use crate::inbox::v1::ParsedBatch;

    proptest! {
        #[test]
        fn sendable_batch_encode_decode(
            transactions in collection::vec(
                collection::vec(Operation::arb_with_signer(), 1..5),
                1..5
            ),
            remaining_input in any::<Vec<u8>>(),
        ) {
            // Arrange
            let batch = Batch::new(
                transactions.into_iter()
                            .map(|ops| Transaction::new(ops).unwrap())
                            .collect()
            );

            let mut b = Vec::new();
            let sig = batch.to_bytes_signed(&mut b).unwrap();

            let mut encoded = Vec::new();
            batch.bin_write(&mut encoded).unwrap();

            encoded.extend_from_slice(remaining_input.as_slice());

            let (remaining, parsed_batch) = ParsedBatch::parse(encoded.as_slice())
                .expect("Parsing of encoded batch failed");

            assert_eq!(remaining_input, remaining, "Parsing batch consumed too many bytes");
            assert_eq!(sig, parsed_batch.aggregated_signature, "Expected signatures to be equal");
        }
    }
}
