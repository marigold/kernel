// SPDX-FileCopyrightText: 2022 TriliTech <contact@trili.tech>
//
// SPDX-License-Identifier: MIT

//! Defines operations over kernel memory - persisted in RAM between yields.

use crate::{
    bls::PublicKey,
    encoding::{
        contract::Contract,
        string_ticket::{StringTicket, StringTicketHash, TrustlessTicketIdentity},
    },
};
use alloc::collections::BTreeMap;
use crypto::hash::Layer2Tz4Hash;
use host::path::RefPath;
use host::rollup_core::RawRollupCore;
use num_bigint::BigInt;
use num_traits::{Signed, Zero};
use serde::{Deserialize, Serialize};
use thiserror::Error;

const MEMORY_PATH: RefPath = RefPath::assert_from(b"/tx/memory");

/// Memory contents, containing ticket definitions & the account balance sheet.
#[derive(Default, Debug, Serialize, Deserialize)]
pub struct Memory {
    tickets: BTreeMap<StringTicketHash, (Contract, String, BigInt)>,
    accounts: Accounts,
}

impl Memory {
    /// Load memory from the durable store.
    pub fn load_memory<Host: RawRollupCore>(host: &Host) -> Self {
        host::runtime::load_value_sized(host, &MEMORY_PATH)
            .map(|mem| {
                serde_json::from_slice(mem.as_slice())
                    .expect("COuld not deserialize memory.")
            })
            .unwrap_or_default()
    }

    /// Snapshot the current state of memory to the store.
    pub fn snapshot<Host: RawRollupCore>(&self, host: &mut Host) {
        let value = serde_json::to_vec(self).expect("Could not serialize memory");

        host::runtime::save_value_sized(host, &MEMORY_PATH, value.as_slice())
    }

    /// Has the ticket has been seen by the kernel before?
    pub fn ticket_recognized(&self, hash: &StringTicketHash) -> bool {
        self.tickets.contains_key(hash)
    }

    /// Add ticket to the kernel's memory.
    ///
    /// If the ticket has been seen before, then just its total balance is updated.
    pub fn add_ticket(&mut self, identity_proof: TrustlessTicketIdentity) {
        if let Some((_, _, ref mut amount)) =
            self.tickets.get_mut(identity_proof.identity())
        {
            *amount += identity_proof.ticket().amount()
        } else {
            let (
                hash,
                StringTicket {
                    creator,
                    contents,
                    amount,
                },
            ) = identity_proof.consume();

            self.tickets
                .insert(hash, (creator, contents, amount.into()));
        }
    }

    /// Reduces the ticket balance in kernel memory.
    ///
    /// Returns the `StringTicket` for the given hash.
    ///
    /// # Panics
    /// Panics if the ticket is not found, or there is insufficient balance to withdraw
    /// the required amount.
    ///
    /// This should only be possible if 'verification' has a bug in it - an account was
    /// credited with more tickets than exist in the 'global tickets table'.
    pub fn withdraw_ticket(
        &mut self,
        hash: StringTicketHash,
        amount: u64,
    ) -> StringTicket {
        match self.tickets.get_mut(&hash) {
            None => panic!("Ticket of hash {:?} not found - unable to withdraw.", hash),
            Some((creator, contents, amount_left)) => {
                *amount_left -= amount;

                if amount_left.is_negative() {
                    panic!(
                        "Ticket {:?} was overdrawn - accounts out of sync.",
                        (creator, contents)
                    )
                }

                if amount_left.is_zero() {
                    // We know that the ticket is in the balance - safe to unwrap.
                    let (creator, contents, _) = self.tickets.remove(&hash).unwrap();

                    StringTicket::new(creator, contents, amount)
                } else {
                    StringTicket::new(creator.clone(), contents.clone(), amount)
                }
            }
        }
    }
    /// Returns a mutable reference to the kernel memory's accounts.
    pub fn accounts_mut(&mut self) -> &mut Accounts {
        &mut self.accounts
    }

    /// Returns a reference to the kernel memory's accounts.
    pub fn accounts(&self) -> &Accounts {
        &self.accounts
    }
}

/// Account balance sheet.
#[derive(Debug, Default, PartialEq, Eq, Serialize, Deserialize)]
pub struct Accounts(BTreeMap<Layer2Tz4Hash, Account>);

impl Accounts {
    /// Get a mutable reference to account.
    pub fn account_of_mut(&mut self, address: &Layer2Tz4Hash) -> Option<&mut Account> {
        self.0.get_mut(address)
    }

    /// Get a mutable reference to account.
    pub fn account_of(&self, address: &Layer2Tz4Hash) -> Option<&Account> {
        self.0.get(address)
    }

    /// Adds a new account at address.
    pub fn add_account(
        &mut self,
        address: Layer2Tz4Hash,
        account: Account,
    ) -> Result<(), AccountError> {
        if self.0.contains_key(&address) {
            return Err(AccountError::AddressOccupied(address));
        }

        self.0.insert(address, account);
        Ok(())
    }

    /// Replaces accounts with their updated version.
    pub fn update_accounts(&mut self, updated_accounts: Self) {
        for (address, account) in updated_accounts.0.into_iter() {
            if let Some(old) = self.account_of_mut(&address) {
                *old = account;
            } else {
                self.0.insert(address, account);
            }
        }
    }
}

/// Errors that may occur when dealing with accounts.
#[derive(Error, Debug)]
pub enum AccountError {
    /// Amount per ticket must fit within `u64`.
    #[error("Adding amount {1} to existing balance {0} would cause overflow")]
    BalanceOverflow(u64, u64),

    /// Ticket balance cannot go negative.
    #[error("Removing amount {1} from existing balance {0} would cause underflow")]
    BalanceUnderflow(u64, u64),

    /// Ticket not held within account
    #[error("Ticket of hash {0:?} not held by account")]
    TicketNotHeld(StringTicketHash),

    /// Address already taken by previous account.
    #[error("Could not add new account due to previous account at address {0}")]
    AddressOccupied(Layer2Tz4Hash),
}

/// Accounts are referenced by their [Layer2Tz4Hash].
///
/// Contain a balance of all tickets held, and optionally the public key associated
/// with the account address.
#[derive(Debug, Clone, Default, PartialEq, Eq, Serialize, Deserialize)]
pub struct Account {
    // TODO: used when handling transactions/withdrawals
    public_key: Option<PublicKey>,
    balance: BTreeMap<StringTicketHash, u64>,
    counter: i64,
}

impl Account {
    /// Add an additional quantity of ticket identified by `hash`, to any already held.
    pub fn add_ticket(
        &mut self,
        hash: StringTicketHash,
        amount: u64,
    ) -> Result<(), AccountError> {
        if let Some(ticket_balance) = self.balance.get_mut(&hash) {
            *ticket_balance = ticket_balance
                .checked_add(amount)
                .ok_or(AccountError::BalanceOverflow(*ticket_balance, amount))?;
        } else {
            self.balance.insert(hash, amount);
        }
        Ok(())
    }

    /// Remove a quantity of ticket identified by `hash`, from balance already held.
    pub fn remove_ticket(
        &mut self,
        hash: &StringTicketHash,
        amount: u64,
    ) -> Result<(), AccountError> {
        if let Some(ticket_balance) = self.balance.get_mut(hash) {
            *ticket_balance = ticket_balance
                .checked_sub(amount)
                .ok_or(AccountError::BalanceUnderflow(*ticket_balance, amount))?;
        } else {
            return Err(AccountError::TicketNotHeld(hash.clone()));
        }
        Ok(())
    }

    /// Set the public key that corresponds to the address of the account.
    pub fn link_public_key(&mut self, pk: PublicKey) {
        self.public_key = Some(pk);
    }

    /// Set the public key that corresponds to the address of the account.
    pub fn public_key(&self) -> Option<&PublicKey> {
        self.public_key.as_ref()
    }

    /// Increments the operation counter of the account.
    pub fn increment_counter(&mut self) {
        self.counter += 1;
    }

    /// The current value of the account's operation counter.
    pub fn counter(&self) -> i64 {
        self.counter
    }
}
