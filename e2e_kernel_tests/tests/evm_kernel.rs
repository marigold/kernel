// SPDX-FileCopyrightText: 2022 TriliTech <contact@trili.tech>
// SPDX-FileCopyrightText: 2022 Nomadic Labs <contact@nomadic-labs.com>
//
// SPDX-License-Identifier: MIT

//! End to end tests for the *EVM kernel*.

#[macro_use]
extern crate kernel;
extern crate alloc;
extern crate evm_kernel;

use crypto::hash::ContractKt1Hash;
use crypto::hash::HashTrait;
use evm_kernel::ethereum::address::EvmAddress;
use evm_kernel::ethereum::basic::{GasLimit, GasPrice, Wei, H256, U256};
use evm_kernel::evm_kernel_run;
use evm_kernel::inbox::v1::sendable::Batch;
use evm_kernel::inbox::v1::sendable::SendableEvmTransaction::EvmCall;
use evm_kernel::inbox::v1::sendable::Transaction::EvmTransaction;
use evm_kernel::inbox::v1::sendable::Transaction::TxTransaction;
use evm_kernel::inbox::v1::sendable::{EthereumCall, ExternalInboxMessage, InboxMessage};
use evm_kernel::inbox::v1::tx::sendable::Transaction as TicketTransaction;
use evm_kernel::inbox::v1::tx::Operation;
use evm_kernel::inbox::v1::tx::OperationContent;
use evm_kernel::inbox::v1::EvmTransactionCommon;
use evm_kernel::inbox::InternalInboxMessage;
use evm_kernel::inbox::Signer;
use evm_kernel::inbox::Transfer;
use evm_kernel::outbox::tx::{OutboxMessage, OutboxMessageTransaction};
use host::rollup_core::Input as InputType;
use kernel_core::bls::BlsKey;
use kernel_core::encoding::contract::Contract;
use kernel_core::encoding::entrypoint::Entrypoint;
use kernel_core::encoding::micheline::MichelineString;
use kernel_core::encoding::michelson::MichelsonPair;
use kernel_core::encoding::public_key_hash::PublicKeyHash;
use kernel_core::encoding::smart_rollup::SmartRollupAddress;
use kernel_core::encoding::string_ticket::StringTicket;
use mock_host::{host_loop, HostInput};
use mock_runtime::state::HostState;
use tezos_encoding::enc::BinWriter;
use tezos_encoding::nom::NomReader;

// Test that we can send a message to call an Ethereum contract.
// We cannot inspect output or state yet. That part will
// be implemented with transaction.
// TODO: <ttps://gitlab.com/tezos/tezos/-/milestones/115>
#[test]
fn call_empty_contract() {
    kernel_entry!(evm_kernel_run);

    // Arrange
    let target_address = EvmAddress::from_u64_be(117u64);
    let call_parameters = EvmTransactionCommon {
        nonce: U256::zero(),
        r#type: 0u8,
        gas_price: GasPrice::zero(), // Arbitrary value
        gas_limit: GasLimit::zero(), // Arbitrary value
        to: target_address,
        value: Wei::zero(), // Arbitrary value
        r: H256::zero(),
        s: H256::zero(),
    };
    let transaction = EvmTransaction(EvmCall(EthereumCall {
        common: call_parameters,
        data: vec![],
    }));
    let batch = Batch::new(vec![transaction]);
    let call_contract = InboxMessage::External(ExternalInboxMessage::V1(batch));

    let mut call_contract_message = vec![];

    call_contract.bin_write(&mut call_contract_message).unwrap();

    // Act
    let init = HostState::default();

    let host_next = |level: i32| -> HostInput {
        if level > 1 {
            HostInput::Exit
        } else {
            HostInput::NextLevel(1)
        }
    };

    let input_messages = |level: i32| -> Vec<(InputType, Vec<u8>)> {
        if level == 1 {
            vec![(InputType::MessageData, call_contract_message.clone())]
        } else {
            vec![]
        }
    };

    let _final_state = host_loop(init, mock_kernel_run, host_next, input_messages);
}

// Test that we can call a precompiled contract by sending a message to the rollup,
// have it parse the message and make the call. We can only inspect output and state,
// after transactions have been implemented.
// TODO <https://gitlab.com/tezos/tezos/-/milestones/115>
#[test]
fn call_identity_precompiled_contract() {
    kernel_entry!(evm_kernel_run);

    // Arrange
    let target_address = EvmAddress::from_u64_be(7u64);
    let call_parameters = EvmTransactionCommon {
        nonce: U256::zero(),
        r#type: 0u8,
        gas_price: GasPrice::zero(), // Arbitrary value
        gas_limit: GasLimit::zero(), // Arbitrary value
        to: target_address,
        value: Wei::zero(), // Arbitrary value
        r: H256::zero(),
        s: H256::zero(),
    };
    let transaction = EvmTransaction(EvmCall(EthereumCall {
        common: call_parameters,
        data: vec![],
    }));
    let batch = Batch::new(vec![transaction]);
    let call_contract = InboxMessage::External(ExternalInboxMessage::V1(batch));

    let mut call_contract_message = vec![];

    call_contract.bin_write(&mut call_contract_message).unwrap();

    // Act
    let init = HostState::default();

    let host_next = |level: i32| -> HostInput {
        if level > 1 {
            HostInput::Exit
        } else {
            HostInput::NextLevel(1)
        }
    };

    let input_messages = |level: i32| -> Vec<(InputType, Vec<u8>)> {
        if level == 1 {
            vec![(InputType::MessageData, call_contract_message.clone())]
        } else {
            vec![]
        }
    };

    let _final_state = host_loop(init, mock_kernel_run, host_next, input_messages);
}

// Test adapted from the transactions kernel. We must still support the transaction
// kernel functionality in the EVM kernel.
#[test]
fn deposit_then_withdraw_to_same_address() {
    kernel_entry!(evm_kernel_run);

    // Arrange
    let bls_key = BlsKey::from_ikm([1; 32]);
    let signer = Signer::BlsPublicKey(bls_key.compressed_public_key().clone());
    let address = bls_key.public_key_hash().clone();

    let originator = Contract::Originated(
        ContractKt1Hash::from_b58check("KT1ThEdxfUcWUwqsdergy3QnbCWGHSUHeHJq").unwrap(),
    );
    let contents = "Hello, Ticket!".to_string();

    let string_ticket = StringTicket::new(originator.clone(), contents.clone(), 500);

    let sender =
        ContractKt1Hash::from_b58check("KT1PWx2mnDueood7fEmfbBDKx1D9BAnnXitn").unwrap();

    let source =
        PublicKeyHash::from_b58check("tz1Ke2h7sDdakHJQh8WX4Z372du1KChsksyU").unwrap();

    let destination =
        SmartRollupAddress::from_b58check("scr1HLXM32GacPNDrhHDLAssZG88eWqCUbyLF")
            .unwrap();

    // Deposit
    let deposit = InboxMessage::Internal(InternalInboxMessage::Transfer(Transfer {
        payload: MichelsonPair(
            MichelineString(address.to_b58check()),
            string_ticket.into(),
        ),
        sender: sender.clone(),
        source,
        destination,
    }));

    let mut deposit_message = Vec::new();
    deposit.bin_write(&mut deposit_message).unwrap();

    // Withdrawal
    let string_ticket = StringTicket::new(
        Contract::Originated(
            ContractKt1Hash::from_b58check("KT1ThEdxfUcWUwqsdergy3QnbCWGHSUHeHJq")
                .unwrap(),
        ),
        "Hello, Ticket!".to_string(),
        450,
    );
    let withdrawal = OperationContent::withdrawal(
        sender.clone(),
        string_ticket,
        Entrypoint::default(),
    );
    let operation = Operation {
        signer,
        counter: 0,
        contents: vec![withdrawal],
    };
    let transaction = TicketTransaction::new([(operation, bls_key.clone())])
        .expect("Valid Transaction");
    let batch = Batch::new(vec![TxTransaction(transaction)]);
    let message = InboxMessage::External(ExternalInboxMessage::V1(batch));
    let mut withdrawal_message = Vec::new();

    message.bin_write(&mut withdrawal_message).unwrap();

    // Act
    let init = HostState::default();

    let host_next = |level: i32| -> HostInput {
        if level > 1 {
            HostInput::Exit
        } else {
            HostInput::NextLevel(1)
        }
    };

    let input_messages = |level: i32| -> Vec<(InputType, Vec<u8>)> {
        if level == 1 {
            vec![
                (InputType::MessageData, deposit_message.clone()),
                (InputType::MessageData, withdrawal_message.clone()),
            ]
        } else {
            vec![]
        }
    };

    let final_state = host_loop(init, mock_kernel_run, host_next, input_messages);

    // Assert: withdrawal outbox message at level one
    let outputs: Vec<_> = final_state
        .store
        .as_ref()
        .iter()
        .filter(|(k, _)| k.starts_with("/output") && k.as_str() != "/output/id")
        .collect();

    assert_eq!(1, outputs.len(), "There should be a single outbox message");

    let path = outputs[0].0;
    assert_eq!("/output/1/0", path);
    let outbox_message = outputs[0].1;

    let (remaining, outbox_message) = OutboxMessage::nom_read(outbox_message.as_slice())
        .expect("Parsing outbox message should work");

    assert!(
        remaining.is_empty(),
        "Unexpected excess bytes in outbox message"
    );

    let transactions = match outbox_message {
        OutboxMessage::AtomicTransactionBatch(transactions) => transactions,
    };
    assert_eq!(1, transactions.len(), "Expected single outbox transaction");

    let string_ticket = StringTicket::new(
        Contract::Originated(
            ContractKt1Hash::from_b58check("KT1ThEdxfUcWUwqsdergy3QnbCWGHSUHeHJq")
                .unwrap(),
        ),
        "Hello, Ticket!".to_string(),
        450,
    );
    let expected = OutboxMessageTransaction {
        parameters: string_ticket.into(),
        destination: Contract::Originated(sender),
        entrypoint: Entrypoint::default(),
    };

    assert_eq!(expected, transactions[0]);
}

// Test coverring:
// - ability to withdraw tickets in an account
// - ability to transfer tickets between accounts, as part of a transaction
// - transactions should fail when there is insufficient balance to perform an operation
// Adapted from the transaction kernel tests - this functionality must still be
// supported by the EVM kernel
#[test]
fn deposit_transfer_withdraw() {
    kernel_entry!(evm_kernel_run);

    // Arrange

    // signer representing first account
    let fst_bls_key = BlsKey::from_ikm([1; 32]);
    let fst_signer_key =
        Signer::BlsPublicKey(fst_bls_key.compressed_public_key().clone());
    let fst_address = fst_bls_key.public_key_hash().clone();

    // signer representing second account
    let snd_bls_key = BlsKey::from_ikm([2; 32]);
    let snd_signer_key =
        Signer::BlsPublicKey(snd_bls_key.compressed_public_key().clone());
    let snd_address = snd_bls_key.public_key_hash().clone();

    let originator = Contract::Originated(
        ContractKt1Hash::from_b58check("KT1ThEdxfUcWUwqsdergy3QnbCWGHSUHeHJq").unwrap(),
    );

    // A ticket that will be deposited into account 1, and transferred to account 2
    let make_fst_ticket = |amount: u64| {
        StringTicket::new(originator.clone(), "Hello, ticket".to_string(), amount)
    };
    // A ticket that will be deposited into account 2, and transferred to account 1
    let make_snd_ticket = |amount: u64| {
        StringTicket::new(originator.clone(), "Goodbye, ticket".to_string(), amount)
    };

    let sender =
        ContractKt1Hash::from_b58check("KT1PWx2mnDueood7fEmfbBDKx1D9BAnnXitn").unwrap();

    let source =
        PublicKeyHash::from_b58check("tz1Ke2h7sDdakHJQh8WX4Z372du1KChsksyU").unwrap();

    let destination =
        SmartRollupAddress::from_b58check("scr1HLXM32GacPNDrhHDLAssZG88eWqCUbyLF")
            .unwrap();

    // Deposit

    // deposit first ticket into account 1
    let fst_deposit = InboxMessage::Internal(InternalInboxMessage::Transfer(Transfer {
        payload: MichelsonPair(
            MichelineString(fst_address.to_b58check()),
            make_fst_ticket(300).into(),
        ),
        sender: sender.clone(),
        source: source.clone(),
        destination: destination.clone(),
    }));

    let mut fst_deposit_message = Vec::new();
    fst_deposit.bin_write(&mut fst_deposit_message).unwrap();

    // deposit second ticket into account 2
    let snd_deposit = InboxMessage::Internal(InternalInboxMessage::Transfer(Transfer {
        payload: MichelsonPair(
            MichelineString(snd_address.to_b58check()),
            make_snd_ticket(300).into(),
        ),
        sender: sender.clone(),
        source: source.clone(),
        destination: destination.clone(),
    }));

    let mut snd_deposit_message = Vec::new();
    snd_deposit.bin_write(&mut snd_deposit_message).unwrap();

    // Withdrawal

    // withdraw 100 of the first ticket
    let fst_withdrawal = || {
        OperationContent::withdrawal(
            sender.clone(),
            make_fst_ticket(100),
            Entrypoint::default(),
        )
    };
    // withdraw 98 of the second ticket
    let snd_withdrawal = || {
        OperationContent::withdrawal(
            sender.clone(),
            make_snd_ticket(98),
            Entrypoint::default(),
        )
    };

    // Transfer
    let snd_to_fst_account_transfer =
        || OperationContent::transfer(fst_address.clone(), make_snd_ticket(99));
    let fst_to_snd_account_transfer =
        || OperationContent::transfer(snd_address.clone(), make_fst_ticket(150));

    // Invalid, as account 2 has not transferred the 'snd_ticket' to account 1
    // before account 1 withdraws.
    let invalid_transaction = TicketTransaction::new([
        (
            Operation {
                signer: fst_signer_key.clone(),
                counter: 0,
                contents: vec![
                    fst_to_snd_account_transfer(),
                    fst_withdrawal(),
                    snd_withdrawal(),
                ],
            },
            fst_bls_key.clone(),
        ),
        (
            Operation {
                signer: snd_signer_key.clone(),
                counter: 0,
                contents: vec![
                    snd_to_fst_account_transfer(),
                    fst_withdrawal(),
                    snd_withdrawal(),
                ],
            },
            snd_bls_key.clone(),
        ),
    ])
    .unwrap();

    let valid_transfer = TicketTransaction::new([
        (
            Operation {
                signer: fst_signer_key.clone(),
                counter: 0,
                contents: vec![fst_to_snd_account_transfer(), fst_withdrawal()],
            },
            fst_bls_key.clone(),
        ),
        (
            Operation {
                signer: snd_signer_key.clone(),
                counter: 0,
                contents: vec![snd_to_fst_account_transfer(), snd_withdrawal()],
            },
            snd_bls_key.clone(),
        ),
    ])
    .unwrap();

    let valid_final_withdraw = TicketTransaction::new([
        (
            Operation {
                signer: Signer::Layer2Address(fst_address.clone()), // bls key now in cache
                counter: 1,
                contents: vec![snd_withdrawal()],
            },
            fst_bls_key.clone(),
        ),
        (
            Operation {
                signer: Signer::Layer2Address(snd_address.clone()), // bls key now in cache
                counter: 1,
                contents: vec![fst_withdrawal()],
            },
            snd_bls_key.clone(),
        ),
    ])
    .unwrap();

    // Act
    let init = HostState::default();

    let invalid_batch = Batch::new(vec![TxTransaction(invalid_transaction)]);
    let invalid_message = InboxMessage::External(ExternalInboxMessage::V1(invalid_batch));
    let mut invalid_external_message = Vec::new();
    invalid_message
        .bin_write(&mut invalid_external_message)
        .unwrap();

    let valid_batch = Batch::new(vec![
        TxTransaction(valid_transfer),
        TxTransaction(valid_final_withdraw),
    ]);
    let valid_message = InboxMessage::External(ExternalInboxMessage::V1(valid_batch));
    let mut valid_external_message = Vec::new();
    valid_message
        .bin_write(&mut valid_external_message)
        .unwrap();

    let host_next = |level: i32| -> HostInput {
        if level > 1 {
            HostInput::Exit
        } else {
            HostInput::NextLevel(1)
        }
    };

    let input_messages = |level: i32| -> Vec<(InputType, Vec<u8>)> {
        if level == 1 {
            vec![
                (InputType::MessageData, fst_deposit_message.clone()),
                (InputType::MessageData, invalid_external_message.clone()),
                (InputType::MessageData, snd_deposit_message.clone()),
                (InputType::MessageData, valid_external_message.clone()),
            ]
        } else {
            vec![]
        }
    };

    let final_state = host_loop(init, mock_kernel_run, host_next, input_messages);

    // Assert: withdrawal outbox message at level one
    let mut outputs: Vec<_> = final_state
        .store
        .as_ref()
        .iter()
        .filter(|(k, _)| k.starts_with("/output") && k.as_str() != "/output/id")
        .collect();
    outputs.sort();

    assert_eq!(2, outputs.len(), "There should be two outbox messages");

    // First withdrawal:
    // - withdraw ticket 1 from account 1
    // - withdraw ticket 2 from account 2
    let path = outputs[0].0;
    assert_eq!("/output/1/0", path);
    let outbox_message = outputs[0].1;

    let (remaining, outbox_message) = OutboxMessage::nom_read(outbox_message.as_slice())
        .expect("Parsing outbox message should work");
    assert!(
        remaining.is_empty(),
        "Unexpected excess bytes in outbox message"
    );

    let transactions = match outbox_message {
        OutboxMessage::AtomicTransactionBatch(transactions) => transactions,
    };
    assert_eq!(2, transactions.len(), "Expected single outbox transaction");

    let fst_expected = OutboxMessageTransaction {
        parameters: make_fst_ticket(100).into(),
        destination: Contract::Originated(sender.clone()),
        entrypoint: Entrypoint::default(),
    };

    let snd_expected = OutboxMessageTransaction {
        parameters: make_snd_ticket(98).into(),
        destination: Contract::Originated(sender.clone()),
        entrypoint: Entrypoint::default(),
    };

    assert_eq!(fst_expected, transactions[0]);
    assert_eq!(snd_expected, transactions[1]);

    // Second withdrawal:
    // - withdraw ticket 2 from account 1
    // - withdraw ticket 1 from account 2
    let path = outputs[1].0;
    assert_eq!("/output/1/1", path);
    let outbox_message = outputs[1].1;

    let (remaining, outbox_message) = OutboxMessage::nom_read(outbox_message.as_slice())
        .expect("Parsing outbox message should work");
    assert!(
        remaining.is_empty(),
        "Unexpected excess bytes in outbox message"
    );

    let transactions = match outbox_message {
        OutboxMessage::AtomicTransactionBatch(transactions) => transactions,
    };
    assert_eq!(2, transactions.len(), "Expected single outbox transaction");

    let fst_expected = OutboxMessageTransaction {
        parameters: make_snd_ticket(98).into(),
        destination: Contract::Originated(sender.clone()),
        entrypoint: Entrypoint::default(),
    };

    let snd_expected = OutboxMessageTransaction {
        parameters: make_fst_ticket(100).into(),
        destination: Contract::Originated(sender.clone()),
        entrypoint: Entrypoint::default(),
    };

    assert_eq!(fst_expected, transactions[0]);
    assert_eq!(snd_expected, transactions[1]);
}
