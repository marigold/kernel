// SPDX-FileCopyrightText: 2022 TriliTech <contact@trili.tech>
//
// SPDX-License-Identifier: MIT

//! Contains entrypoint of the SCORU wasm kernel.
//!
//! A kernel must expose a `fn kernel_run();` entrypoint, which is called on a loop
//! by the runtime.  The kernel *yields* to the runtime by returning out of
//! `kernel_run`.
//!
//! There is a limit on how many computation ticks a kernel may perform per entry. It is
//! called a number of times per non-empty level.  The kernel must take care not to perform
//! arbitrarily long computations, to avoid breaching the computation limit.
#![deny(missing_docs)]
#![deny(rustdoc::all)]
#![allow(unused_variables)]
#![allow(dead_code)]
#![cfg_attr(not(feature = "std"), no_std)]

#[cfg(feature = "dlmalloc")]
mod allocator {
    use dlmalloc::GlobalDlmalloc;

    #[global_allocator]
    static ALLOCATOR: GlobalDlmalloc = GlobalDlmalloc;
}

/// Set panic hook
#[cfg(feature = "panic-hook")]
pub fn set_panic_hook() {
    std::panic::set_hook(Box::new(panic_handler::panic_handler));
}

#[cfg(feature = "dlmalloc")]
extern crate alloc;

/// Derive `kernel_run` & `mock_kernel_run` entrypoints.
///
/// ```should_panic
/// # extern crate alloc;
/// #[macro_use] extern crate kernel;
/// #[macro_use] extern crate debug;
///
/// use host::rollup_core::RawRollupCore;
///
/// fn run<Host: RawRollupCore>(_host: &mut Host) {
///   debug_msg!(Host, "Hello: {}", "Kernel!");
/// }
///
/// kernel_entry!(run);
///
/// loop {
///   kernel_run();
/// }
/// ```
#[macro_export]
macro_rules! kernel_entry {
    ($kernel_run: expr) => {
        /// The `kernel_run` function is called by the wasm host at regular intervals.
        #[cfg(target_arch = "wasm32")]
        #[no_mangle]
        pub extern "C" fn kernel_run() {
            #[cfg(feature = "panic-hook")]
            kernel::set_panic_hook();
            let mut host = unsafe { host::wasm_host::WasmHost::new() };
            $kernel_run(&mut host)
        }

        /// The `kernel_run` function is called by the wasm host at regular intervals.
        #[cfg(not(target_arch = "wasm32"))]
        pub fn kernel_run() {
            panic!(
                "kernel_run is only supported on 'target = \"wasm32\"', \
                 use mock_kernel_run instead"
            );
        }

        /// The `mock_kernel_run` is called by the mock host at regular intervals.
        #[cfg(not(target_arch = "wasm32"))]
        pub fn mock_kernel_run(host: &mut mock_runtime::host::MockHost) {
            #[cfg(feature = "panic-hook")]
            kernel::set_panic_hook();

            $kernel_run(host)
        }
    };
}
