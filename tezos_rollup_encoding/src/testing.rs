// SPDX-FileCopyrightText: 2022 TriliTech <contact@trili.tech>

//! Helper functions for setting up DAC testing environment

#![cfg(feature = "testing")]
use crypto::blake2b::digest_256;
use host::rollup_core::PREIMAGE_HASH_SIZE;
use num_bigint::BigInt;
use num_traits::FromPrimitive;
use tezos_encoding::types::Zarith;

/// Hashes `content` into a preimage hash
pub fn make_preimage_hash(content: &[u8]) -> [u8; PREIMAGE_HASH_SIZE] {
    let hash: [u8; PREIMAGE_HASH_SIZE - 1] = digest_256(content)
        .expect("hashing failed")
        .try_into()
        .expect("hash is incorrect length");
    let mut root_hash: [u8; PREIMAGE_HASH_SIZE] = [0; PREIMAGE_HASH_SIZE];
    root_hash[1..].copy_from_slice(&hash);
    root_hash
}

/// Converts bitset (as usize) into Zarith.
pub fn make_witnesses(bitset: usize) -> Zarith {
    Zarith(BigInt::from_usize(bitset).unwrap())
}
