#!/usr/bin/env bash

# SPDX-FileCopyrightText: 2022 TriliTech <contact@trili.tech>
#
# SPDX-License-Identifier: MIT

source $(git rev-parse --show-toplevel)/scripts/cargo-docker.sh

cargo fmt -- --check &&\
    cargo clippy &&\
    cargo test --features testing &&\
    cargo build --target wasm32-unknown-unknown &&\
    cargo doc
