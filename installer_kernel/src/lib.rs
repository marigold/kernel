// SPDX-FileCopyrightText: 2022 TriliTech <contact@trili.tech>
// SPDX-FileCopyrightText: 2022 PK Lab <info@pklab.io>
//
// SPDX-License-Identifier: MIT

//! When originating a _WebAssembly Smart Rollup_ on Tezos, you must specify the full kernel
//! (as hex-encoded WebAssembly) to be executed. Since this operation takes place on Layer-1,
//! this places a limit of *32KB* on the size of the origination kernel.
//!
//! Most useful kernels will be significantly larger than this, however, due to requiring
//! certain runtime attributes such as _allocation_, _formatting_, and _cryptographic
//! verification_.
//!
//! Therefore, so-called _installer kernels_ are provided - that are small enough to be
//! used as an origination kernel, that contain logic to install the kernel actually desired
//! by the rollup originator. This is done through the _kernel upgrade mechanism_.
#![cfg_attr(not(test), no_std)]
#![deny(rustdoc::all)]
#![allow(unused)]

use core::panic::PanicInfo;

#[macro_use]
extern crate kernel;

pub mod reveal_installer;

#[cfg(feature = "preimage-installer")]
pub mod preimage_installer {
    //! The preimage installer installs the kernel preimage from the root hash.
    use crate::reveal_installer::install_kernel;
    use debug::debug_str;
    use host::rollup_core::{RawRollupCore, PREIMAGE_HASH_SIZE};

    /// The preimage root hash that the installer will request.
    ///
    /// This is a placeholder value, it is possible to compile the installer kernel,
    /// and replace this with whatever root hash you choose, that corresponds to the
    /// kernel you wish to install.
    pub const ROOT_PREIMAGE_HASH: &[u8; PREIMAGE_HASH_SIZE * 2] =
        b"1acaa995ef84bc24cc8bb545dd986082fbbec071ed1c3e9954abea5edc441ccd3a";

    kernel_entry!(reveal_installer);

    /// Attempts to install the kernel corresponding to [ROOT_PREIMAGE_HASH].
    pub fn reveal_installer<Host: RawRollupCore>(host: &mut Host) {
        if let Err(err) = install_kernel(host, ROOT_PREIMAGE_HASH) {
            debug_str!(Host, "Installation failed:");
            debug_str!(Host, err);
        } else {
            debug_str!(Host, "Kernel installed.")
        }
    }
}

#[cfg(feature = "tx-demo-installer")]
pub mod tx_demo_installer {
    //! The tx demo installer installs the kernel preimage from the root hash.
    //!
    //! Additionally, it also sets the `rollup_id` for the kernel (between 0-999),
    //! and the public keys of the DAC committee.
    use crate::reveal_installer::install_kernel;
    use debug::debug_str;
    use host::path::RefPath;
    use host::rollup_core::{RawRollupCore, PREIMAGE_HASH_SIZE};
    use host::runtime::Runtime;

    // TODO: bls has this constant, but kernel_core can't be pulled in
    // without using alloc - bls constants should be moved out of kernel_core.
    const COMPRESSED_PK_SIZE: usize = 48;

    // move bls
    const TX_CONFIG_SIZE: usize =
        (PREIMAGE_HASH_SIZE + core::mem::size_of::<u16>() + COMPRESSED_PK_SIZE) * 2;

    const KERNEL_ID_INDEX: usize = PREIMAGE_HASH_SIZE * 2;
    const DAC_PK_INDEX: usize = KERNEL_ID_INDEX + core::mem::size_of::<u16>() * 2;

    /// The config required for setting up the tx-demo kernel.
    ///
    /// This is a placeholder value, it is possible to compile the installer kernel,
    /// and replace this with whatever root hash you choose, that corresponds to the
    /// kernel you wish to install.
    ///
    /// The config is made up of the following:
    /// - hex-encoded root preimage hash, corresponding to the tx kernel.
    /// - hex-encoded kernel-id (between 0-999 inclusive), as a little-endian u16.
    /// - hex-encoded bls public key (compressed: 48 bytes) for the demo DAC committee
    ///   member.
    pub const TX_CONFIG: &[u8; TX_CONFIG_SIZE] =
        b"1acaa995ef84bc24cc8bb545dd986082fbbec071ed1c3e9954abea5edc441ccd3a\
          0000\
          555555555555555555555555555555555555555555555555\
          555555555555555555555555555555555555555555555555";

    kernel_entry!(tx_demo_installer);

    const KERNEL_ID_PATH: RefPath = RefPath::assert_from(b"/kernel/id");
    // TODO: replace with `dac_committee`, `_-` now allowed in paths by PVM,
    //       but `host::path` hasn't been updated yet.
    const DAC_COMMITTEE_MEMBER_PATH: RefPath =
        RefPath::assert_from(b"/kernel/dac.committee/0");

    /// Attempts to install the kernel corresponding to [ROOT_PREIMAGE_HASH].
    pub fn tx_demo_installer<Host: RawRollupCore>(host: &mut Host) {
        let root_hash: &[u8; PREIMAGE_HASH_SIZE * 2] =
            TX_CONFIG[..KERNEL_ID_INDEX].try_into().unwrap();

        write_kernel_id(host);
        write_kernel_dac_committee(host);

        let _ = install_kernel(host, root_hash);
    }

    fn write_kernel_id<Host: RawRollupCore>(host: &mut Host) {
        let kernel_id_hex = &TX_CONFIG[KERNEL_ID_INDEX..DAC_PK_INDEX];
        let mut kernel_id = [0; 2];

        let _ = hex::decode_to_slice(&kernel_id_hex[..4], &mut kernel_id);
        let _ = Runtime::store_write(host, &KERNEL_ID_PATH, &kernel_id, 0);
    }

    fn write_kernel_dac_committee<Host: RawRollupCore>(host: &mut Host) {
        let kernel_dac_hex = &TX_CONFIG[DAC_PK_INDEX..];
        let mut dac_pk = [0; 48];

        let _ = hex::decode_to_slice(kernel_dac_hex, &mut dac_pk);
        let _ = Runtime::store_write(host, &DAC_COMMITTEE_MEMBER_PATH, &dac_pk, 0);
    }
}

/// Panic handler used when targetting wasm.
///
/// Any error when installing a kernel is fatal, therefore we
/// handle panics by panicking again, which aborts execution.
#[cfg_attr(all(target_arch = "wasm32", not(feature = "std")), panic_handler)]
fn panic(_info: &PanicInfo) -> ! {
    panic!()
}
