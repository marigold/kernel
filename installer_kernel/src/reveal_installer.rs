// SPDX-FileCopyrightText: 2022 TriliTech <contact@trili.tech>
//
// SPDX-License-Identifier: MIT

//! The *Preimage Installer* installs a kernel, that is too large to be originated directly.
//!
//! It does so leveraging both *DAC* and the [reveal preimage] mechanism.
//!
//! At its limit, 3 levels of DAC pages gives us enough content for >7GB, which is already
//! far in excess of the likely max limit on WASM modules that the PVM can support.
//! Store values alone are limited to ~2GB.
//!
//! [reveal preimage]: host::rollup_core::reveal_preimage
use debug::debug_str;
use host::path::RefPath;
use host::rollup_core::{RawRollupCore, MAX_FILE_CHUNK_SIZE, PREIMAGE_HASH_SIZE};
use host::runtime::{Runtime, RuntimeError};
use tezos_rollup_encoding::dac::{
    reveal_loop, SlicePage, V0SliceContentPage, MAX_PAGE_SIZE,
};
const KERNEL_PATH: RefPath = RefPath::assert_from(b"/kernel/boot.wasm");
const PREPARE_KERNEL_PATH: RefPath = RefPath::assert_from(b"/installer/kernel/boot.wasm");

// Support 3 levels of hashes pages, and then bottom layer of content.
const MAX_DAC_LEVELS: usize = 4;

/// Installs the kernel corresponding to the hex-encoded root hash.
pub fn install_kernel<Host: RawRollupCore>(
    host: &mut Host,
    root_hex_hash: &[u8; PREIMAGE_HASH_SIZE * 2],
) -> Result<(), &'static str> {
    let root_hash = decode_root_hash(root_hex_hash)?;

    // 16KB buffer
    let mut buffer = [0; MAX_PAGE_SIZE * MAX_DAC_LEVELS];
    let mut kernel_size = 0;
    let mut write_page_to_store = save_content(&mut kernel_size);
    reveal_loop(
        host,
        0,
        &root_hash,
        buffer.as_mut_slice(),
        MAX_DAC_LEVELS,
        &mut write_page_to_store,
    )?;

    Runtime::store_move(host, &PREPARE_KERNEL_PATH, &KERNEL_PATH)
        .map_err(|_| "FAILED to install kernel in KERNEL_PATH")?;

    Ok(())
}

fn save_content<'a, Host: RawRollupCore>(
    kernel_size: &'a mut usize,
) -> impl FnMut(&mut Host, V0SliceContentPage) -> Result<(), &'static str> + '_ {
    |host, page| write_content(host, kernel_size, page)
}

/// Appends the content of the page to the kernel being installed.
fn write_content<Host: RawRollupCore>(
    host: &mut Host,
    kernel_size: &mut usize,
    content: V0SliceContentPage,
) -> Result<(), &'static str> {
    use core::ops::AddAssign;

    let content = content.as_ref();

    let mut size_written = 0;
    while size_written < content.len() {
        let num_to_write =
            usize::min(MAX_FILE_CHUNK_SIZE, (content.len() - size_written));
        let bytes_to_write = &content[size_written..(size_written + num_to_write)];

        Runtime::store_write(host, &PREPARE_KERNEL_PATH, bytes_to_write, *kernel_size)
            .map_err(|_| "Failed to write kernel content page")?;

        size_written += num_to_write;

        *kernel_size += num_to_write;
    }

    Ok(())
}

fn decode_root_hash(
    hex_hash: &[u8; PREIMAGE_HASH_SIZE * 2],
) -> Result<[u8; PREIMAGE_HASH_SIZE], &'static str> {
    let mut root_hash = [0; PREIMAGE_HASH_SIZE];
    hex::decode_to_slice(hex_hash, root_hash.as_mut_slice())
        .map_err(|_| "ROOT_PREIMAGE_HASH must be valid HEX.")?;

    Ok(root_hash)
}

#[cfg(test)]
mod tests {
    use super::*;
    use mock_runtime::host::MockHost;
    use mock_runtime::state::HostState;
    use tezos_encoding::enc::BinWriter;
    use tezos_rollup_encoding::dac::{Page, V0ContentPage, V0HashPage, MAX_PAGE_SIZE};

    fn prepare_preimages(
        state: &mut HostState,
        input: &[u8],
    ) -> [u8; PREIMAGE_HASH_SIZE] {
        let content_pages = V0ContentPage::new_pages(input)
            .map(Page::V0ContentPage)
            .map(|page| {
                let mut buffer = Vec::with_capacity(MAX_PAGE_SIZE);
                page.bin_write(&mut buffer).expect("can serialize");
                buffer
            });

        let mut hashes =
            Vec::with_capacity(input.len() / V0ContentPage::MAX_CONTENT_SIZE + 1);

        for page in content_pages {
            assert!(page.len() <= 4096);
            let hash = state.set_preimage(page);

            hashes.push(hash);
        }

        let mut hash_pages: Vec<_> = V0HashPage::new_pages(&hashes).collect();
        assert_eq!(1, hash_pages.len(), "expected single hash page");

        let hash_page = hash_pages.remove(0);

        let mut root_page = Vec::with_capacity(MAX_PAGE_SIZE);
        Page::V0HashPage(hash_page)
            .bin_write(&mut root_page)
            .expect("cannot serialize hash page");

        let root_hash = state.set_preimage(root_page);

        root_hash
    }

    #[test]
    pub fn installer_sets_correct_kernel() {
        // Arrange
        let mut host_state = HostState::default();

        let mut kernel: Vec<u8> = Vec::with_capacity(40_000);
        for i in 0..10000 {
            kernel.extend_from_slice(u16::to_le_bytes(i).as_slice());
        }

        let root_hash = prepare_preimages(&mut host_state, &kernel);

        let mut root_hex_hash = [0; PREIMAGE_HASH_SIZE * 2];
        hex::encode_to_slice(&root_hash, root_hex_hash.as_mut_slice())
            .expect("hex encoding should work");

        let mut mock_host = MockHost::from(host_state);

        // Act
        let result = install_kernel(&mut mock_host, &root_hex_hash);

        if let Err(e) = result {
            panic!("ERROR: {}", e);
        }

        // Assert
        assert!(result.is_ok());

        let installed_kernel: Vec<u8> = mock_host
            .into_inner()
            .store
            .get_value("/durable/kernel/boot.wasm");

        assert_eq!(installed_kernel, kernel, "Expected kernel to be installed.");
    }
}
