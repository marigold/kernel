// SPDX-FileCopyrightText: 2022 TriliTech <contact@trili.tech>
//
// SPDX-License-Identifier: MIT

//! Mock runtime provides a host that can used in integration & unit tests.
#![deny(missing_docs)]
#![deny(rustdoc::all)]

pub mod host;
pub mod state;
pub mod trap;
