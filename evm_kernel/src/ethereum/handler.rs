// SPDX-FileCopyrightText: 2022 TriliTech <contact@trili.tech>
//
// SPDX-License-Identifier: MIT

//! Handle details of EVM runtime
//!
//! The interface between SputnikVM and the kernel. This includes interface
//! to storage, account balances, block constants, _and transaction state_.

use crate::ethereum::block::BlockConstants;
use crate::ethereum::storage;
use crate::ethereum::transaction::Transaction;
use core::convert::Infallible;
use evm::{
    Capture, Context, CreateScheme, ExitError, ExitReason, Handler, Opcode, Stack,
    Transfer,
};
use host::rollup_core::RawRollupCore;
use primitive_types::{H160, H256, U256};

/// The hash of zero code
/// TODO: add this in milestone
/// <https://gitlab.com/tezos/tezos/-/milestones/111>
/// (contract creation).
const ZERO_HASH: H256 = H256::zero();

/// The implementation of the SputnikVM [Handler] trait
pub struct EvmHandler<'a, Host: RawRollupCore> {
    /// The host
    host: &'a mut Host,
    /// Current stack of transactions. Top of stack is last element
    /// of the vector
    pub transaction_stack: Vec<Transaction>,
    /// The constants for the current block
    pub block: &'a BlockConstants,
}

#[allow(unused_variables)]
impl<'a, Host: RawRollupCore> EvmHandler<'a, Host> {
    /// Create a new handler to suit a new, initial EVM call context
    pub fn new(host: &'a mut Host, block: &'a BlockConstants, context: Context) -> Self {
        Self {
            host,
            block,
            transaction_stack: vec![Transaction::new(context)],
        }
    }

    /// Create a handler for a sub-context
    pub fn sub_transaction(&self, sub_context: Context) -> Self {
        todo!("https://gitlab.com/tezos/tezos/-/milestones/109")
    }

    /// Go through the transaction stack youngest to oldest sub transaction.
    /// Return first value where [f] returns [Some]. Return [None] otherwise.
    fn scan_transaction_stack<A, F>(&self, f: F) -> Option<A>
    where
        F: Fn(&Transaction) -> Option<A>,
    {
        self.transaction_stack.iter().rev().find_map(f)
    }
}

#[allow(unused_variables)]
impl<'a, Host: RawRollupCore> Handler for EvmHandler<'a, Host> {
    type CreateInterrupt = Infallible;
    type CreateFeedback = Infallible;
    type CallInterrupt = Infallible;
    type CallFeedback = Infallible;

    fn balance(&self, address: H160) -> U256 {
        self.scan_transaction_stack(|t| t.balance(address))
            .or_else(|| storage::balance(self.host, address))
            .unwrap_or(U256::zero())
    }

    fn code_size(&self, address: H160) -> U256 {
        self.scan_transaction_stack(|t| t.code_size(address))
            .or_else(|| storage::code_size(self.host, address))
            .unwrap_or(U256::zero())
    }

    fn code_hash(&self, address: H160) -> H256 {
        self.scan_transaction_stack(|t| t.code_hash(address))
            .or_else(|| storage::code_hash(self.host, address))
            .unwrap_or(ZERO_HASH)
    }

    fn code(&self, address: H160) -> Vec<u8> {
        self.scan_transaction_stack(|t| t.code(address))
            .or_else(|| storage::code(self.host, address))
            .unwrap_or_default()
    }

    fn storage(&self, address: H160, index: H256) -> H256 {
        self.scan_transaction_stack(|t| t.get_storage(address, index))
            .or_else(|| storage::get_value(self.host, address, index))
            .unwrap_or(H256::zero())
    }

    fn original_storage(&self, address: H160, index: H256) -> H256 {
        storage::get_value(self.host, address, index).unwrap_or(H256::zero())
    }

    fn gas_left(&self) -> U256 {
        todo!("To be done when we do gas and transactions. No milestone or issue yet")
    }

    fn gas_price(&self) -> U256 {
        self.block.gas_price.value.into()
    }

    fn origin(&self) -> H160 {
        todo!("Milestone: https://gitlab.com/tezos/tezos/-/milestones/115")
    }

    fn block_hash(&self, number: U256) -> H256 {
        let v = number.low_u32() as usize;

        if v < 256 {
            // Because of this "feature" of Ethereum, we cannot do this
            // operation while processing transactions for the first 255 blocks.
            // Ethereum fixed this by running for a while before users got access
            // (and their promised eth).
            *self.block.hash.get(v).unwrap()
        } else {
            todo!("Milestone: https://gitlab.com/tezos/tezos/-/milestones/115")
        }
    }

    fn block_number(&self) -> U256 {
        self.block.number
    }

    fn block_coinbase(&self) -> H160 {
        self.block.coinbase
    }

    fn block_timestamp(&self) -> U256 {
        self.block.timestamp
    }

    fn block_difficulty(&self) -> U256 {
        self.block.difficulty
    }

    fn block_gas_limit(&self) -> U256 {
        self.block.gas_limit.value.into()
    }

    fn block_base_fee_per_gas(&self) -> U256 {
        self.block.base_fee_per_gas.value.into()
    }

    fn chain_id(&self) -> U256 {
        self.block.chain_id
    }

    fn exists(&self, address: H160) -> bool {
        self.code_size(address) > U256::zero()
    }

    fn deleted(&self, address: H160) -> bool {
        todo!("https://gitlab.com/tezos/tezos/-/milestones/109")
    }

    fn is_cold(&self, address: H160, index: Option<H256>) -> bool {
        todo!("https://gitlab.com/tezos/tezos/-/milestones/109")
    }

    fn set_storage(
        &mut self,
        address: H160,
        index: H256,
        value: H256,
    ) -> Result<(), ExitError> {
        todo!("https://gitlab.com/tezos/tezos/-/milestones/109")
    }

    fn log(
        &mut self,
        address: H160,
        topics: Vec<H256>,
        data: Vec<u8>,
    ) -> Result<(), ExitError> {
        todo!("https://gitlab.com/tezos/tezos/-/milestones/109")
    }

    fn mark_delete(&mut self, address: H160, target: H160) -> Result<(), ExitError> {
        todo!("https://gitlab.com/tezos/tezos/-/milestones/109")
    }

    fn create(
        &mut self,
        caller: H160,
        scheme: CreateScheme,
        value: U256,
        init_code: Vec<u8>,
        target_gas: Option<u64>,
    ) -> Capture<(ExitReason, Option<H160>, Vec<u8>), Self::CreateInterrupt> {
        todo!("https://gitlab.com/tezos/tezos/-/milestones/111")
    }

    fn call(
        &mut self,
        code_address: H160,
        transfer: Option<Transfer>,
        input: Vec<u8>,
        target_gas: Option<u64>,
        is_static: bool,
        context: Context,
    ) -> Capture<(ExitReason, Vec<u8>), Self::CallInterrupt> {
        todo!("https://gitlab.com/tezos/tezos/-/milestones/115")
    }

    fn pre_validate(
        &mut self,
        context: &Context,
        opcode: Opcode,
        stack: &Stack,
    ) -> Result<(), ExitError> {
        Ok(()) // TODO - this is a stub
    }

    fn create_feedback(
        &mut self,
        _feedback: Self::CreateFeedback,
    ) -> Result<(), ExitError> {
        todo!("https://gitlab.com/tezos/tezos/-/milestones/111")
    }
}
