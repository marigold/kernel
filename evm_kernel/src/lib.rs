// SPDX-FileCopyrightText: 2022 TriliTech <contact@trili.tech>
// SPDX-FileCopyrightText: 2022 Nomadic Labs <contact@nomadic-labs.com>
//
// SPDX-License-Identifier: MIT

//! Contains the EVM kernel
//!
//! This kernel runs EVM contract code emulating Ethereum, but on a rollup.
//! Alongside the EVM functionality, the kernel also implements **TORU** style
//! transactions kernel functionality.
#![deny(missing_docs)]
#![deny(rustdoc::all)]
#![forbid(unsafe_code)]

extern crate alloc;

pub mod deposit;
pub mod ethereum;
pub mod inbox;
pub mod memory;
pub mod outbox;
pub mod transactions;

use crate::inbox::v1::tx::verifiable::VerifiedTransaction as VerifiedTxTransaction;
use crate::inbox::v1::verifiable::VerifiedTransaction;
use crate::inbox::v1::ParsedEvmTransaction::{EvmCall, EvmCreate, EvmTransfer};
use crate::transactions::tx::withdrawal::handle_withdrawals;
use debug::debug_msg;
use deposit::tx::{deposit_ticket, DepositError};
use host::input::Input;
use host::rollup_core::{
    RawRollupCore, MAX_INPUT_MESSAGE_SIZE, MAX_INPUT_SLOT_DATA_CHUNK_SIZE,
};
use host::runtime::Runtime;
use inbox::DepositFromInternalPayloadError;
use tezos_encoding::nom::error::DecodeError;
use thiserror::Error;

use crate::ethereum::block::BlockConstants;
use crate::ethereum::{call_evm, EvmCallError};
use crate::inbox::{InboxDeposit, InboxMessage, InternalInboxMessage, Transfer};
use crate::memory::tx::Memory;
use crate::transactions::external_inbox;
use crate::transactions::external_inbox::ProcessedOutcome;

const MAX_READ_INPUT_SIZE: usize =
    if MAX_INPUT_MESSAGE_SIZE > MAX_INPUT_SLOT_DATA_CHUNK_SIZE {
        MAX_INPUT_MESSAGE_SIZE
    } else {
        MAX_INPUT_SLOT_DATA_CHUNK_SIZE
    };

/// Entrypoint of the *transactions* kernel.
pub fn evm_kernel_run<Host: RawRollupCore>(host: &mut Host) {
    let mut memory = Memory::load_memory(host);
    match host.read_input(MAX_READ_INPUT_SIZE) {
        Ok(Some(Input::Message(message))) => {
            debug_msg!(
                Host,
                "Processing MessageData {} at level {}",
                message.id,
                message.level
            );

            if let Err(err) = process_header_payload(host, &mut memory, message.as_ref())
            {
                debug_msg!(Host, "Error processing header payload {}", err);
            }
        }
        Ok(Some(Input::Slot(_message))) => todo!("handle slot message"),
        Ok(None) => {}
        Err(_) => todo!("handle runtime errors"),
    }
    memory.snapshot(host);
}

#[derive(Error, Debug)]
enum TransactionError<'a> {
    #[error("unable to parse header inbox message {0}")]
    MalformedInboxMessage(nom::Err<DecodeError<&'a [u8]>>),
    #[error("Failed to deposit ticket: {0}")]
    Deposit(#[from] DepositError),
    #[error("Invalid internal inbox message, expected deposit: {0}")]
    InvalidInternalInbox(#[from] DepositFromInternalPayloadError),
    #[error("EVM contract call failed: {0}")]
    EvmCall(#[from] EvmCallError),
}

// TODO: state machine this - on external messages, we should consume 'one at a time'
// - need to return remaining input + instruction to 'keep processing'
fn process_header_payload<'a, Host: RawRollupCore>(
    host: &mut Host,
    memory: &mut Memory,
    payload: &'a [u8],
) -> Result<(), TransactionError<'a>> {
    let current_block = BlockConstants::from_storage::<Host>(host);
    let precompiles = ethereum::precompiles::precompile_set();

    let (remaining, message) =
        InboxMessage::parse(payload).map_err(TransactionError::MalformedInboxMessage)?;

    match message {
        InboxMessage::Internal(InternalInboxMessage::Transfer(Transfer {
            payload,
            ..
        })) => {
            let InboxDeposit {
                destination,
                ticket,
            } = payload.try_into()?;

            deposit_ticket::<Host>(memory, destination, ticket)?;

            // Internal inbox message - not batched
            debug_assert!(remaining.is_empty());
            Ok(())
        }
        InboxMessage::Internal(
            InternalInboxMessage::StartOfLevel | InternalInboxMessage::EndOfLevel,
        ) => Ok(()),
        InboxMessage::External(message) => {
            if let Some(mut process) = external_inbox::prepare_for_processing::<Host>(
                message,
                memory.accounts_mut(),
            ) {
                loop {
                    match process(memory.accounts()) {
                        ProcessedOutcome::More => continue,
                        ProcessedOutcome::Finished => break,
                        ProcessedOutcome::Transaction(verified_transaction) => {
                            match verified_transaction {
                                VerifiedTransaction::TicketTransaction(
                                    VerifiedTxTransaction {
                                        updated_accounts,
                                        withdrawals,
                                    },
                                ) => {
                                    memory
                                        .accounts_mut()
                                        .update_accounts(updated_accounts);

                                    handle_withdrawals(host, memory, withdrawals);
                                }
                                VerifiedTransaction::EthereumTransaction(EvmCall(
                                    call,
                                )) => {
                                    call_evm(
                                        host,
                                        &current_block,
                                        &precompiles,
                                        call.common.to.into(),
                                        call.common.caller().into(),
                                        &call.data,
                                    )?;
                                }
                                VerifiedTransaction::EthereumTransaction(EvmCreate(
                                    _create,
                                )) => todo!(
                                    "https://gitlab.com/tezos/tezos/-/milestones/111"
                                ),
                                VerifiedTransaction::EthereumTransaction(
                                    EvmTransfer(_transfer),
                                ) => todo!(
                                    "https://gitlab.com/tezos/tezos/-/milestones/115"
                                ),
                            }
                        }
                    }
                }
            }

            Ok(())
        }
    }
}

/// Define the `kernel_run` for the transactions kernel.
#[cfg(feature = "evm_kernel")]
pub mod evm_kernel {
    use crate::evm_kernel_run;
    use kernel::kernel_entry;
    kernel_entry!(evm_kernel_run);
}

#[cfg(test)]
mod test {
    use crate::memory::tx::Account;
    use kernel_core::encoding::{
        contract::Contract,
        micheline::MichelineString,
        michelson::{MichelsonContract, MichelsonPair},
        public_key_hash::PublicKeyHash,
        smart_rollup::SmartRollupAddress,
        string_ticket::StringTicket,
    };

    use super::*;

    use crypto::hash::{ContractKt1Hash, HashTrait, Layer2Tz4Hash};
    use host::rollup_core::Input;
    use mock_runtime::host::MockHost;
    use tezos_encoding::enc::BinWriter;

    #[test]
    fn deposit_ticket() {
        // Arrange
        let mut mock_runtime = MockHost::default();

        let ticket_destination =
            Layer2Tz4Hash::from_b58check("tz4MSfZsn6kMDczShy8PMeB628TNukn9hi2K").unwrap();

        let ticket_creator =
            Contract::from_b58check("KT1JW6PwhfaEJu6U3ENsxUeja48AdtqSoekd").unwrap();
        let ticket_content = "hello, ticket!";
        let ticket_quantity = 5;

        let message = InboxMessage::Internal(InternalInboxMessage::Transfer(Transfer {
            payload: MichelsonPair(
                MichelineString(ticket_destination.to_base58_check()),
                MichelsonPair(
                    MichelsonContract(ticket_creator.clone()),
                    MichelsonPair(
                        MichelineString(ticket_content.to_string()),
                        ticket_quantity.into(),
                    ),
                ),
            ),
            sender: ContractKt1Hash::from_b58check(
                "KT1VsSxSXUkgw6zkBGgUuDXXuJs9ToPqkrCg",
            )
            .unwrap(),
            source: PublicKeyHash::from_b58check("tz3SvEa4tSowHC5iQ8Aw6DVKAAGqBPdyK1MH")
                .unwrap(),
            destination: SmartRollupAddress::from_b58check(
                "scr1HLXM32GacPNDrhHDLAssZG88eWqCUbyLF",
            )
            .unwrap(),
        }));

        let mut input = Vec::new();
        message.bin_write(&mut input).unwrap();

        let level = 10;
        mock_runtime.as_mut().set_ready_for_input(level);
        mock_runtime
            .as_mut()
            .add_next_inputs(10, vec![(Input::MessageData, input)].iter());

        // Act
        evm_kernel_run(&mut mock_runtime);

        // Assert
        let memory = Memory::load_memory(&mut mock_runtime);
        let account = memory
            .accounts()
            .account_of(&ticket_destination)
            .cloned()
            .expect("Account should be created");

        let mut expected_account = Account::default();
        let expected_ticket = StringTicket::new(
            ticket_creator,
            ticket_content.to_string(),
            ticket_quantity as u64,
        );
        expected_account
            .add_ticket(expected_ticket.identify().unwrap(), ticket_quantity as u64)
            .unwrap();

        assert_eq!(expected_account, account);
    }

    #[test]
    fn deposit_fails_on_invalid_destination() {
        // Arrange
        let mut mock_runtime = MockHost::default();

        let ticket_destination = "tz4BAD";

        let ticket_creator =
            Contract::from_b58check("KT1JW6PwhfaEJu6U3ENsxUeja48AdtqSoekd").unwrap();
        let ticket_content = "hello, ticket!";
        let ticket_quantity = 5;

        let message = InboxMessage::Internal(InternalInboxMessage::Transfer(Transfer {
            payload: MichelsonPair(
                MichelineString(ticket_destination.to_string()),
                MichelsonPair(
                    MichelsonContract(ticket_creator.clone()),
                    MichelsonPair(
                        MichelineString(ticket_content.to_string()),
                        ticket_quantity.into(),
                    ),
                ),
            ),
            sender: ContractKt1Hash::from_b58check(
                "KT1VsSxSXUkgw6zkBGgUuDXXuJs9ToPqkrCg",
            )
            .unwrap(),
            source: PublicKeyHash::from_b58check("tz3SvEa4tSowHC5iQ8Aw6DVKAAGqBPdyK1MH")
                .unwrap(),
            destination: SmartRollupAddress::from_b58check(
                "scr1HLXM32GacPNDrhHDLAssZG88eWqCUbyLF",
            )
            .unwrap(),
        }));

        let mut input = Vec::new();
        message.bin_write(&mut input).unwrap();

        let level = 10;
        mock_runtime.as_mut().set_ready_for_input(level);
        mock_runtime
            .as_mut()
            .add_next_inputs(10, vec![(Input::MessageData, input)].iter());

        // Act
        evm_kernel_run(&mut mock_runtime);

        // Assert
        let ticket_identity = StringTicket::new(
            ticket_creator,
            ticket_content.to_string(),
            ticket_quantity as u64,
        )
        .identify()
        .unwrap();

        let memory = Memory::load_memory(&mock_runtime);
        assert!(!memory.ticket_recognized(&ticket_identity));
    }
}
