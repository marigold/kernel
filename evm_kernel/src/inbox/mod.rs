// SPDX-FileCopyrightText: 2022 TriliTech <contact@trili.tech>
// SPDX-FileCopyrightText: 2022 Nomadic Labs <contact@nomadic-labs.com>
//
// SPDX-License-Identifier: MIT

//! Types a encodings for the *inbox* for the EVM kernel
//!
//! The inbox allows for all messages also allowed by the transactions kernel.
//! Alongside this, it accepts EVM transactions as transactions in batches in
//! external messages.

use nom::bytes::complete::tag;
use nom::combinator::map;
use nom::sequence::preceded;

pub use kernel_core::inbox::DepositFromInternalPayloadError;
pub use kernel_core::inbox::InboxDeposit;
pub use kernel_core::inbox::InboxMessage;
pub use kernel_core::inbox::InternalInboxMessage;
pub use kernel_core::inbox::Transfer;

pub use kernel_core::inbox::external::ExternalInboxMessage;
pub use kernel_core::inbox::Signer;

pub mod v1;

/// A de-serializaed external message
///
/// Such message can include all elements found in external messages for the
/// transaction kernel. Additionally, it may include calls to the EVM runtime
/// for contract calls, contract creation, and Ethereum transfer operations.
pub enum ParsedEvmExternalInboxMessage<'a> {
    /// Version 1 - including v1 messages from tx kernel
    V1(v1::ParsedEvmBatch<'a>),
}

impl<'a> ParsedEvmExternalInboxMessage<'a> {
    /// Parse the external message bytes
    pub fn parse(input: &'a [u8]) -> tezos_encoding::nom::NomResult<Self> {
        map(
            preceded(tag([0_u8]), v1::ParsedEvmBatch::parse),
            ParsedEvmExternalInboxMessage::V1,
        )(input)
    }
}
