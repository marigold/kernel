// SPDX-FileCopyrightText: 2022 TriliTech <contact@trili.tech>
//
// SPDX-License-Identifier: MIT

//! The version 1 inbox used in the transactions kernel.

pub use kernel_core::inbox::external::v1::*;
