# SPDX-FileCopyrightText: 2022 TriliTech <contact@trili.tech>
#
# SPDX-License-Identifier: MIT

FROM index.docker.io/rust:1.60

ARG CARGO_HOME=/opt/.cargo
ENV PATH=${CARGO_HOME}/bin:${PATH}

RUN apt-get update &&\
    apt-get install -y clang &&\
    rustup target add wasm32-unknown-unknown &&\
    rustup component add clippy &&\
    rustup component add rustfmt &&\
    cargo install cargo-make

ENV CARGO_HOME=/usr/cargo
WORKDIR /usr/src/
